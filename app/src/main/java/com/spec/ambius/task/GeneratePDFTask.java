package com.spec.ambius.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.spec.ambius.common.util.SaveImageUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GeneratePDFTask extends AsyncTask<String, Void, Object> {

    public static final String EMPTY_STRING = "";
    public static final int RESULT_OK = 1;
    public static final int RESULT_EXCEPTION = 2;

    private String pdfPath = "";
    private Context context;
    private Handler handler;
    private ProgressDialog prdDialog;
    private Map<Integer, Object> returnedMap;
    private HashMap<String,ArrayList<String>> categoryItemMap = new HashMap<>(0);
    private ArrayList<String> iteams;
    private int counter =0;
    /**
     * Constructor.
     *
     * @param context Context object of the caller activity.
     */
    public GeneratePDFTask(Context context, Handler handler, HashMap<String,ArrayList<String>> categoryItemMap) {
        this.context = context;
        this.handler = handler;
        this.returnedMap = new HashMap<Integer, Object>();
        this.categoryItemMap = categoryItemMap;
    }

    @Override
    protected void onPreExecute() {
        prdDialog = ProgressDialog.show(context, EMPTY_STRING,
                "Please wait...");
    }

    @Override
    protected Object doInBackground(String... params) {
        Document document = new Document();
        PdfReader pdfReader = null;
        // PdfStamper pdfStamper = null;

        File outputFile = null;
        try {
            Date date = new Date(System.currentTimeMillis());
            pdfPath = context.getExternalFilesDir(null) + File.separator + "img_rentokilambius_" + SaveImageUtil.getTime(date) + ".pdf";
            outputFile = new File(pdfPath);
            copyAssets(context);
            pdfReader = new PdfReader(context.getExternalFilesDir(null) + File.separator + "Ambius.pdf");
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(outputFile));
            //          pdfStamper = new PdfStamper(pdfReader,
//                    new FileOutputStream(outputFile));

            document.open();

            InputStream ims = new FileInputStream(new File(
                    params[0]));
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            com.itextpdf.text.Image image = null;

            image = com.itextpdf.text.Image
                    .getInstance(stream.toByteArray());
            image.setAlignment(Element.ALIGN_CENTER);
            image.scalePercent(25);


            //PdfContentByte content = pdfStamper.getUnderContent(1);
            PdfContentByte content = writer.getDirectContent();
            PdfImportedPage page = writer.getImportedPage(pdfReader, 1);

            document.newPage();
            content.addTemplate(page, 0, 0);
            image.setAbsolutePosition(50f, 440f);
            content.addImage(image);

            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
            Font categoryTextFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);

            boolean isCommentAvailable = false;
            int yAxis = 430;
            int noOfLinesInComment = params[1].split("\\n").length;
            if (params[1] != null && !params[1].isEmpty()) {

                ColumnText ct = new ColumnText(content);
                Phrase headerComment = new Phrase("Comment: ", boldFont);
                ct.setSimpleColumn(headerComment, 50, yAxis, 580, 317, 15, Element.ALIGN_LEFT);
                ct.go();
                Phrase comments = new Phrase(params[1]);
                ct.setSimpleColumn(comments, 50, yAxis - 15, 580, 317, 15, Element.ALIGN_LEFT);
                ct.go();
                isCommentAvailable = true;
            }
            int y;
            if (isCommentAvailable) {
                //y = yAxis - 100;
                y = yAxis - 35 - noOfLinesInComment * 20;
            } else {
                y = yAxis;
            }
            ColumnText columnText = new ColumnText(content);
            Phrase headerInfo = new Phrase("Catalogues: ", boldFont);
            columnText.setSimpleColumn(headerInfo, 50, y, 580, 317, 15, Element.ALIGN_LEFT);
            columnText.go();
            Phrase prase;
            ColumnText ct;
            for (String key : categoryItemMap.keySet() ) {
                y = y - 20;
                ct = new ColumnText(content);
                prase = new Phrase(key, categoryTextFont);
                ct.setSimpleColumn(prase, 50, y, 580, 100, 15, Element.ALIGN_LEFT);
                //System.out.println("Name:" + key);
                //System.out.println("Category : Y:" + y);
                ct.go();
                iteams = new ArrayList<>(0);
                iteams = categoryItemMap.get(key);
                if (iteams.size() > 0) {
                    int k;
                    Phrase myText1;
                    ColumnText ctext = new ColumnText(content);
                    for (k = 0; k < iteams.size(); k++) {
                        y -= 20;
                        counter++;
                        //System.out.println("K :"+ k +" Iteam : Y:" +y);
                        myText1 = new Phrase((k+1) +". "+iteams.get(k));
                        ctext.setSimpleColumn(myText1, 50, y, 580, 100, 15, Element.ALIGN_LEFT);
                        ctext.go();
                        if (y < 160 ) {
                            document.newPage();
                            content.addTemplate(page, 0, 0);
                            y = 700;
                            counter = 0;
                        }

                    }
                }

            }

/*            ColumnText columnText = new ColumnText(content);
            Phrase headerInfo = new Phrase("Catalogues: ", boldFont);
            columnText.setSimpleColumn(headerInfo, 50, y, 580, 317, 15, Element.ALIGN_LEFT);
            columnText.go();

            if (params[2] != null && !params[2].isEmpty()) {
                String[] items = params[2].split(",,");
                if (items.length > 0) {
                    y = y - 15;
                    int k;
                    ColumnText ct = new ColumnText(content);
                    for (k = 0; k < items.length; k++) {
                        Phrase myText1 = new Phrase(items[k]);
                        ct.setSimpleColumn(myText1, 50, y, 580, 100, 15, Element.ALIGN_LEFT);
                        y -= 20;
                        ct.go();
                        if (k == 6) {
                            if (!(k + 1 == items.length)) {
                                document.newPage();
                                content.addTemplate(page, 0, 0);
                                y = 700;
                            }
                        }
                    }
                }
            }*/

/*            if (params[2] != null && !params[2].isEmpty()) {
                String[] items = params[2].split(",,");
                if (items.length > 0) {
                    document.newPage();
                    content.addTemplate(page, 0, 0);

                    int y = 700;
                    for (int k = 0; k < items.length; k++) {
                        Phrase myText1 = new Phrase(items[k]);
                        ct.setSimpleColumn(myText1, 75, y, 580, 200, 15, Element.ALIGN_LEFT);
                        y -= 20;
                        ct.go();
                    }
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                document.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        returnedMap.put(RESULT_OK, outputFile.getAbsoluteFile().getAbsolutePath());
        return returnedMap;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        prdDialog.dismiss();
        if (!((HashMap<Integer, Object>) result).containsKey(RESULT_EXCEPTION)) {
            Message message = new Message();
            Bundle bundle = new Bundle();
            bundle.putString("file", pdfPath);
            message.setData(bundle);
            handler.sendMessage(message);
        } else {
            Toast.makeText(context, "Problem in creating PDF!", Toast.LENGTH_SHORT).show();
        }
    }

    private void copyAssets(Context context) {
        AssetManager assetManager = context.getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open("Ambius.pdf");
            out = new FileOutputStream(context.getExternalFilesDir(null) + File.separator + "Ambius.pdf");
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

}
