package com.spec.ambius.view;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.UserRecoverableAuthException;
import com.spec.ambius.R;
import com.spec.ambius.common.callbackinterface.IOnUserRecoverableException;
import com.spec.ambius.common.constants.Constants;
import com.spec.ambius.common.constants.Global;
import com.spec.ambius.common.util.GoogleSheetUtility;
import com.spec.ambius.common.util.SaveImageUtil;
import com.spec.ambius.common.util.Util;

import java.io.File;
import java.util.Date;

public class CreateArtActivity extends TitleActivity implements View.OnClickListener {

    private TextView txtCaptureCamera, txtImportGallery, txtPhoneStorage,txtLogOut;
    //    private DatabaseManager databaseManager;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private RelativeLayout watermarkRelativeLayout;

    private File outputFileFromCamera;

    private GoogleSheetUtility googleSheetUtility;
    private static final int RECOVERY_REQUEST_CODE = 8888;
    private boolean isNeedToInsertLog = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_create_art);
        setActionBarIcon(R.drawable.ic_launcher);

        txtCaptureCamera = (TextView) findViewById(R.id.txtCameraCapture);
        txtImportGallery = (TextView) findViewById(R.id.txtImportGallery);
        txtPhoneStorage = (TextView) findViewById(R.id.txtPhoneStorage);
        txtLogOut = (TextView) findViewById(R.id.txtLogOut);
        watermarkRelativeLayout = (RelativeLayout) findViewById(R.id.watermark_relative_layout);
        /*Drawable watermarkDrawable = getResources().getDrawable(R.drawable.ic_watermark);
        watermarkDrawable.setAlpha(10);
        watermarkRelativeLayout.setBac*/

        watermarkRelativeLayout.getBackground().setAlpha(75);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            boolean fromPreviewScreen = bundle.getBoolean("from_preview");
            if (fromPreviewScreen) {
                txtPhoneStorage.setVisibility(View.VISIBLE);
            }
            isNeedToInsertLog = bundle.getBoolean(Constants.IS_NEED_TO_INSERT);
            if (Util.getConnectivityStatusString(CreateArtActivity.this).equals(Constants.TYPE_NOT_CONNECTED_STRING)) {
                Util.showToast(CreateArtActivity.this,getResources().getString(R.string.msg_internet_connection));
            }else{
                if(isNeedToInsertLog){
                    SharedPreferences prefs = getSharedPreferences(Constants.MyPREFERENCES, MODE_PRIVATE);
                    String emailAddress = prefs.getString(Constants.USER_EMAIL_PREF, null);

                    if(emailAddress != null && !emailAddress.equals("")){
                        googleSheetUtility = new GoogleSheetUtility(iOnUserRecoverableException, getString(R.string.app_name_google_console), Constants.SPREADSHEET_FEED_URL, CreateArtActivity.this);
                        if(googleSheetUtility != null){
                            googleSheetUtility.insertLoginSpreadSheetEntries(emailAddress,handler);
                        }else{
                            Util.showToast(CreateArtActivity.this, Constants.ERROR_WHILE_CONFIGURING);
                        }
                    }else{
                        Toast.makeText(CreateArtActivity.this, Constants.ACCOUNT_NAME_NULL_OR_EMPTY, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }


        txtCaptureCamera.setOnClickListener(this);
        txtImportGallery.setOnClickListener(this);
        txtPhoneStorage.setOnClickListener(this);
        txtLogOut.setOnClickListener(this);
  /*      databaseManager = new DatabaseManager(this);
        if (!databaseManager.isColorAvailable())
            databaseManager.insertColors();
*/

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

        editor.putBoolean("isActivityPaused", false);
        editor.commit();
        editor.putBoolean("isEmailBtnClicked", false);
        editor.commit();

    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == Constants.INSERT_ITEM_DETAILS){

            }else if(msg.what == Constants.NO_SPREAD_SHEET_AVAILABLE){
                Util.showMessage(CreateArtActivity.this, getResources().getString(R.string.msg_spread_sheet_not_exist));
                //Toast.makeText(PreviewActivity.this,"Ambius VIP data spreadsheet doesn't exist",Toast.LENGTH_SHORT).show();
            }else if(msg.what == Constants.NO_SPREAD_TAB_AVAILABLE){
                Util.showMessage(CreateArtActivity.this,getResources().getString(R.string.msg_logindetails_sheet_not_exist));
                //Toast.makeText(PreviewActivity.this,"Tab name 'itemdetail' doesn't exist in Ambius VIP data spreadsheet",Toast.LENGTH_SHORT).show();
            }else if(msg.what == Constants.NO_EDIT_ACCESS){
                Util.showMessage(CreateArtActivity.this,getResources().getString(R.string.msg_not_access));
            }else if(msg.what == Constants.NO_USER_EMAIL){
                Util.showMessage(CreateArtActivity.this, getResources().getString(R.string.msg_useremail_in_login_not_exist));
            }else if(msg.what == Constants.NO_DATE_TIME){
                Util.showMessage(CreateArtActivity.this,getResources().getString(R.string.msg_datetime_in_login_not_exist));
            }else if(msg.what == Constants.INTERNET_CONNECTION){
                Util.showMessage(CreateArtActivity.this, getResources().getString(R.string.msg_internet_connection));
            }
        }
    };

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_create_art;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtCameraCapture:
                capturePhoto();
                break;
            case R.id.txtImportGallery:
                getImageFromGallery();
                break;
            case R.id.txtPhoneStorage:
                startActivity(new Intent(CreateArtActivity.this, GalleryActivity.class));
                break;
            case R.id.txtLogOut:
                SharedPreferences sharedpreferences = getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(CreateArtActivity.this,AuthenticationActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void getImageFromGallery() {
        Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryintent.setType("image/*");
        startActivityForResult(galleryintent, Constants.IMAGE_PICK);
    }

    private void capturePhoto() {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Date date = new Date(System.currentTimeMillis());
        Global.capturedFileName = "original_image_" + SaveImageUtil.getTime(date) + ".jpg";
        //out = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), Global.capturedFileName);
        if (SaveImageUtil.isExternalStrorageAvailable()) {
            File folder = new File(Environment.getExternalStorageDirectory() + "/Ambius-VIP");
            if (!folder.exists()) {
                folder.mkdir();
            }
            String fileName = folder.getAbsolutePath()
                    + File.separator + Global.capturedFileName;
            outputFileFromCamera = new File(fileName);
        } else {
            String fileName = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                    + File.separator + Global.capturedFileName;
            outputFileFromCamera = new File(fileName);
        }

        i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outputFileFromCamera));
        startActivityForResult(i, Constants.CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.IMAGE_PICK:
                if (resultCode == RESULT_OK) {
                    Intent intent = new Intent(CreateArtActivity.this, MainActivity.class);
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    String filePath = Util.getPath(CreateArtActivity.this, selectedImage);
                    intent.putExtra("filePath", filePath);
                    intent.putExtra("isCaptured", false);
                    startActivity(intent);
                }
                break;
            case Constants.CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    Intent intent = new Intent(CreateArtActivity.this, MainActivity.class);
                    intent.putExtra("filePath", outputFileFromCamera.getAbsolutePath());
                    intent.putExtra("isCaptured", true);
                    startActivity(intent);
                }
                break;
            case RECOVERY_REQUEST_CODE:
                    if (resultCode == RESULT_OK) {
                        if (data != null) {
                            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                            if (accountName != null && !accountName.equals("")) {
                                googleSheetUtility.insertLoginSpreadSheetEntries(accountName,handler);
                            } else {
                                Toast.makeText(CreateArtActivity.this, Constants.ACCOUNT_NAME_NULL_OR_EMPTY, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(CreateArtActivity.this, Constants.INTENT_DATA_NULL, Toast.LENGTH_SHORT).show();
                        }
                    } else if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(CreateArtActivity.this, Constants.CHOOSER_CANCELLED, Toast.LENGTH_SHORT).show();

                    }

        }
    }

    IOnUserRecoverableException iOnUserRecoverableException = new IOnUserRecoverableException() {
        @Override
        public void onUserRecoverableException(UserRecoverableAuthException exception) {
            // this app to have permission to read my spreadsheet."
            Intent recoveryIntent = exception.getIntent();
            startActivityForResult(recoveryIntent, RECOVERY_REQUEST_CODE);
        }
    };

}
