package com.spec.ambius.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.UserRecoverableAuthException;
import com.spec.ambius.R;
import com.spec.ambius.common.adapter.CustomEmailChooserAdapter;
import com.spec.ambius.common.callbackinterface.IOnUserRecoverableException;
import com.spec.ambius.common.constants.Constants;
import com.spec.ambius.common.parser.CatalogueJsonParser;
import com.spec.ambius.common.util.GoogleSheetUtility;
import com.spec.ambius.common.util.SaveImageUtil;
import com.spec.ambius.common.util.Util;
import com.spec.ambius.task.GeneratePDFTask;
import com.spec.ambius.vo.ArtVO;
import com.spec.ambius.vo.EditTextVO;
import com.spec.ambius.vo.ItemVO;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PreviewActivity extends TitleActivity implements AdapterView.OnItemClickListener,
        MediaScannerConnection.MediaScannerConnectionClient {


    private String TAG = "PreviewActivity";

    private ArtVO artVO;
    private ImageView imgPreview;
    private LinearLayout imageDetailsLayout;
    private ArrayList<String> applicationPackageNamesList;
    private ArrayList<String> composeActivitiesNamesList;
    private AlertDialog.Builder builder;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String selectedEmailClientPackageName;
    private String selectedEmailClientComposeActivity;
    private AlertDialog dialog;
    private EditText toEmailId, comment;
    private ArrayList<ItemVO> potList;
    private ArrayList<EditTextVO> editTexts;
    private Resources resources;
    private static HashMap<String,ArrayList<String>> categoryItemMap;
    private ArrayList<String> iteamList = new ArrayList<>(0);
    private HashMap<Integer, ItemVO> potMap;
    private GoogleSheetUtility googleSheetUtility;

    private MediaScannerConnection mediaScannerConnection;
    private static final int RECOVERY_REQUEST_CODE = 8888;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_preview);
        setActionBarIcon(R.drawable.ic_launcher);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);
        imageDetailsLayout = (LinearLayout) findViewById(R.id.layoutDetail);
        resources = getResources();
        Bundle extras = getIntent().getExtras();
        mediaScannerConnection = new MediaScannerConnection(this, this);
        if (extras != null) {
            artVO = extras.getParcelable("artVO");
            loadImage(artVO.getEditedImagePath());
        }
        try{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mediaScannerConnection.connect();
                }
            }).start();
        }catch (Exception e){
            e.printStackTrace();
        }

        toEmailId = (EditText) findViewById(R.id.to_mailId);
        comment = (EditText) findViewById(R.id.comment);
        applicationPackageNamesList = new ArrayList<String>();
        composeActivitiesNamesList = new ArrayList<String>();
        builder = new AlertDialog.Builder(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        potMap = (HashMap<Integer, ItemVO>) getIntent().getSerializableExtra("potmap");
        Iterator iterator = potMap.entrySet().iterator();
        potList = new ArrayList<ItemVO>(0);
        editTexts = new ArrayList<>(0);
        ItemVO potVO;
        while (iterator.hasNext()) {
            Map.Entry potEntry = (Map.Entry) iterator.next();
              potVO= (ItemVO) potEntry.getValue();
            if (potVO != null) {
                if (potVO.getItemId() != 212) {
                    potList.add(potVO);
                    generateView(potVO);
                }
            }
            System.out.println("Preview Screen Total Size ::: " + potList.size());
        }

        comment.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER
                        && event.getAction() == KeyEvent.ACTION_UP) {

                    String text = ((EditText) v).getText().toString();

                    int editTextRowCount = text.split("\\n").length;

                    if (editTextRowCount >= 3) {

                        int lastBreakIndex = text.lastIndexOf("\n");

                        String newText = text.substring(0, lastBreakIndex);

                        ((EditText) v).setText("");
                        ((EditText) v).append(newText + " ");
                    }
                }
                return false;
            }

        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_preview;
    }

    private void loadImage(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        //decode first with inJustDecodeBounds to get dimensions
        BitmapFactory.decodeFile(path, options);

        options.inPreferredConfig = Bitmap.Config.ALPHA_8;
        //options.inSampleSize = SaveImageUtil.calculateInSampleSize(options, 200, 200);
        options.inSampleSize = SaveImageUtil.calculateInSampleSize(options, resources.getInteger(R.integer.art_size), resources.getInteger(R.integer.art_size));
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        imgPreview.setImageBitmap(bitmap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_preview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send:
                if (Util.getConnectivityStatusString(PreviewActivity.this).equals(Constants.TYPE_NOT_CONNECTED_STRING)) {
                    Util.showToast(PreviewActivity.this,getResources().getString(R.string.msg_internet_connection));
                }else{
                    String targetEmail = toEmailId.getText().toString();
                    if (targetEmail != null && !targetEmail.isEmpty()) {

                        if(Util.isValidEmail(targetEmail)){
                            if(categoryItemMap != null){
                                categoryItemMap = null;
                            }
                            if(iteamList != null && iteamList.size() > 0){
                                iteamList.clear();
                            }
                            getPotInfoForPDF();
                            googleSheetUtility = new GoogleSheetUtility(iOnUserRecoverableException, getString(R.string.app_name_google_console), Constants.SPREADSHEET_FEED_URL, PreviewActivity.this);
                            if(googleSheetUtility != null){
                                SharedPreferences prefs = getSharedPreferences(Constants.MyPREFERENCES, MODE_PRIVATE);
                                String emailAddress = prefs.getString(Constants.USER_EMAIL_PREF, null);
                                googleSheetUtility.insertItemDetailSpreadSheetEntries(emailAddress,targetEmail,categoryItemMap,handler);
                            }
                        }else{
                            Toast.makeText(this, resources.getString(R.string.enter_valid_email), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(this, resources.getString(R.string.enter_email), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void generateView(ItemVO potVO) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);

        TextView potNameTxtView = new TextView(PreviewActivity.this);
        potNameTxtView.setLayoutParams(layoutParams);
        potNameTxtView.setText(potVO.getItemName());
        linearLayout.addView(potNameTxtView);
        int parentId = CatalogueJsonParser.getTopParent(this, potVO);
        switch (parentId) {
            case 1:
                EditText colorEdtText = new EditText(PreviewActivity.this);
                colorEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
                colorEdtText.setHint(resources.getString(R.string.enter_color));
                if (potVO.getColorId() == -1) {
                    colorEdtText.setText("Default Color");
                } else {
                    if (potVO.getColorName() != null) {
                        colorEdtText.setText(potVO.getColorName());
                    } else {
                        colorEdtText.setText("Default Color");
                    }
                }
                colorEdtText.setSingleLine(true);
                colorEdtText.setTag("Color");
                linearLayout.addView(colorEdtText);
                editTexts.add(new EditTextVO(colorEdtText, false));

                EditText sizeEdtText = new EditText(PreviewActivity.this);
                sizeEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
                sizeEdtText.setHint(resources.getString(R.string.enter_size));
                sizeEdtText.setTag("Size");
                sizeEdtText.setSingleLine(true);
                linearLayout.addView(sizeEdtText);
                editTexts.add(new EditTextVO(sizeEdtText, true));
                break;
            case 2:
                EditText typeEdtText = new EditText(PreviewActivity.this);
                typeEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f));
                typeEdtText.setHint(resources.getString(R.string.enter_type));
                typeEdtText.setTag("Type");
                typeEdtText.setSingleLine(true);
                linearLayout.addView(typeEdtText);
                editTexts.add(new EditTextVO(typeEdtText, true));
                break;
            case 3:
                EditText rangeEdtText = new EditText(PreviewActivity.this);
                rangeEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f));
                rangeEdtText.setHint(resources.getString(R.string.enter_range));
                rangeEdtText.setTag("Range");
                rangeEdtText.setSingleLine(true);
                linearLayout.addView(rangeEdtText);
                editTexts.add(new EditTextVO(rangeEdtText, true));
                break;
            case 4:
                EditText colorFinishingEdtText = new EditText(PreviewActivity.this);
                colorFinishingEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f));
                colorFinishingEdtText.setHint(resources.getString(R.string.enter_color_finishing));
                colorFinishingEdtText.setTag("Color finishing");
                colorFinishingEdtText.setSingleLine(true);
                linearLayout.addView(colorFinishingEdtText);
                editTexts.add(new EditTextVO(colorFinishingEdtText, true));
                break;
            case 5:
                EditText troughColorEdtText = new EditText(PreviewActivity.this);
                troughColorEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f));
                troughColorEdtText.setHint(resources.getString(R.string.enter_trough_color));
                troughColorEdtText.setTag("Trough color");
                troughColorEdtText.setSingleLine(true);
                linearLayout.addView(troughColorEdtText);
                editTexts.add(new EditTextVO(troughColorEdtText, true));
                break;
            case 6:
                EditText lightLevelEdtText = new EditText(PreviewActivity.this);
                lightLevelEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
                lightLevelEdtText.setHint(resources.getString(R.string.enter_light_level));
                lightLevelEdtText.setTag("Light level");
                lightLevelEdtText.setSingleLine(true);
                linearLayout.addView(lightLevelEdtText);
                editTexts.add(new EditTextVO(lightLevelEdtText, false));

                EditText luxReadingEdtText = new EditText(PreviewActivity.this);
                luxReadingEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
                luxReadingEdtText.setHint(resources.getString(R.string.enter_lux_reading));
                luxReadingEdtText.setTag("Lux Reading");
                luxReadingEdtText.setSingleLine(true);
                linearLayout.addView(luxReadingEdtText);
                editTexts.add(new EditTextVO(luxReadingEdtText, true));
                break;
            case 0:
                Log.d(TAG, "****ERROR****");
                break;
        }
/*        TextView potColorTxtView = new TextView(PreviewActivity.this);
        potColorTxtView.setLayoutParams(layoutParams);
        if (potVO.getColorId() == -1) {
            potColorTxtView.setText("Default Color");
        } else {
            if (potVO.getColorName() != null) {
                potColorTxtView.setText(potVO.getColorName() + " " + potVO.getColorCode());
            } else {
                potColorTxtView.setText("Default Color");
            }


        }
        linearLayout.addView(potColorTxtView);*/

     /*   EditText sizeEdtText = new EditText(PreviewActivity.this);
        sizeEdtText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        sizeEdtText.setHint("Enter size");
        linearLayout.addView(sizeEdtText);
        editTexts.add(sizeEdtText);*/

        //linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        imageDetailsLayout.addView(linearLayout);

    }

    private void sendEmail(final String file) {
        ArrayList<String> applicationNamesList = new ArrayList<String>();
        ArrayList<Drawable> applicationIconsList = new ArrayList<Drawable>();

        final PackageManager pkgManager = getPackageManager();

        SharedPreferences prefs = getSharedPreferences(Constants.MyPREFERENCES, MODE_PRIVATE);
        String emailAddress = prefs.getString(Constants.USER_EMAIL_PREF, null);
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",emailAddress, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");

        List<ResolveInfo> activities = pkgManager.queryIntentActivities(emailIntent, 0);
        for (int i = 0; i < activities.size(); i++) {
            Log.i("Packg name ", activities.get(i).activityInfo.packageName);
            applicationPackageNamesList.add(activities.get(i).activityInfo.packageName);
            applicationNamesList.add(activities.get(i).activityInfo.applicationInfo.loadLabel(pkgManager).toString());
            applicationIconsList.add(activities.get(i).activityInfo.loadIcon(pkgManager));
            composeActivitiesNamesList.add(activities.get(i).activityInfo.name);
            Log.i("Application Info", activities.get(i).activityInfo.applicationInfo.loadLabel(pkgManager).toString());
        }

        builder.setTitle(resources.getString(R.string.chooser));

        View emailClientChooserView = getLayoutInflater().inflate(R.layout.layout_dialog_email_chooser, ((ViewGroup) findViewById(R.id.root_LinearLayout_Dialog_Email_Chooser)), false);

        ListView listViewEmailChooser = (ListView) emailClientChooserView.findViewById(R.id.listViewEmailChooser);
        CustomEmailChooserAdapter adapter = new CustomEmailChooserAdapter(PreviewActivity.this, applicationNamesList, applicationIconsList);
        if (listViewEmailChooser != null && adapter != null) {
            listViewEmailChooser.setAdapter(adapter);
        }

        listViewEmailChooser.setOnItemClickListener(this);

        builder.setView(emailClientChooserView);
        builder.setPositiveButton(Constants.JUST_ONCE, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (selectedEmailClientPackageName != null && selectedEmailClientComposeActivity != null) {
                    sentEmailUsingIntent(file);
                }
            }
        });

        builder.setNegativeButton(Constants.ALWAYS, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor.putBoolean(Constants.CHECK_PREFERENCE, true);
                editor.putString(Constants.OPEN_PREFFERED_EMAIL_CLIENT, selectedEmailClientPackageName);
                editor.putString(Constants.PREFFERED_MAIL_CLIENT_COMPOSE_ACTIVITY, selectedEmailClientComposeActivity);
                editor.commit();

                if (selectedEmailClientPackageName != null && selectedEmailClientComposeActivity != null) {
                    sentEmailUsingIntent(file);
                }
            }
        });

        dialog = builder.create();
        if (sharedPreferences.getBoolean(Constants.CHECK_PREFERENCE, false)) {
            selectedEmailClientPackageName = sharedPreferences.getString(Constants.OPEN_PREFFERED_EMAIL_CLIENT, Constants.DEFAULT_PACKAGE_NAME);
            selectedEmailClientComposeActivity = sharedPreferences.getString(Constants.PREFFERED_MAIL_CLIENT_COMPOSE_ACTIVITY, Constants.DEFAULT_COMPOSE_ACTIVITY);
            if (selectedEmailClientPackageName != null && selectedEmailClientComposeActivity != null) {
                sentEmailUsingIntent(file);
            }
        } else {
            dialog.show();
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setEnabled(false);
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                    .setEnabled(false);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.listViewEmailChooser) {
            selectedEmailClientPackageName = applicationPackageNamesList.get(position);
            selectedEmailClientComposeActivity = composeActivitiesNamesList.get(position);
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(true);
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == Constants.INSERT_ITEM_DETAILS){
                GeneratePDFTask task = new GeneratePDFTask(PreviewActivity.this, handler,categoryItemMap);
                task.execute(artVO.getEditedImagePath(), comment.getText().toString());
            }else if(msg.what == Constants.NO_SPREAD_SHEET_AVAILABLE){
                Util.showMessage(PreviewActivity.this, getResources().getString(R.string.msg_spread_sheet_not_exist));
                //Toast.makeText(PreviewActivity.this,"Ambius VIP data spreadsheet doesn't exist",Toast.LENGTH_SHORT).show();
            }else if(msg.what == Constants.NO_SPREAD_TAB_AVAILABLE){
                Util.showMessage(PreviewActivity.this, getResources().getString(R.string.msg_itemdetails_sheet_not_exist));
                //Toast.makeText(PreviewActivity.this,"Tab name 'itemdetail' doesn't exist in Ambius VIP data spreadsheet",Toast.LENGTH_SHORT).show();
            }else if(msg.what == Constants.NO_EDIT_ACCESS){
                Util.showMessage(PreviewActivity.this, getResources().getString(R.string.msg_not_access));
            }else if(msg.what == Constants.NO_USER_EMAIL){
                Util.showMessage(PreviewActivity.this, getResources().getString(R.string.msg_useremail_in_item_not_exist));
            }else if(msg.what == Constants.NO_DATE_TIME){
                Util.showMessage(PreviewActivity.this, getResources().getString(R.string.msg_datetime_in_item_not_exist));
            }else if(msg.what == Constants.NO_CATEGORY){
                Util.showMessage(PreviewActivity.this, getResources().getString(R.string.msg_category_in_item_not_exist));
            }else if(msg.what == Constants.NO_ITEM_NAME){
                Util.showMessage(PreviewActivity.this,getResources().getString(R.string.msg_itemname_in_item_not_exist));
            }else if(msg.what == Constants.NO_TARGET_EMAIL){
                Util.showMessage(PreviewActivity.this,getResources().getString(R.string.msg_target_in_item_not_exist));
            }else if(msg.what == Constants.INTERNET_CONNECTION){
                Util.showMessage(PreviewActivity.this, getResources().getString(R.string.msg_internet_connection));
            }else{
                if(msg.getData().getString("file")!= null){
                    sendEmail(msg.getData().getString("file"));
                }
            }
        }
    };

    private String getPotInformation() {
        try {
            String potInformation = "";
            for (int i = 0, j = 0; i < potList.size() && j < editTexts.size(); i++, j++) {
                ItemVO potVO = potList.get(i);
                EditTextVO editTextVO = editTexts.get(j);
                potInformation = potInformation +(i+1) + ".  Item name: " + potVO.getItemName() +
                        ",\n\t\t" + editTextVO.getEditText().getTag() + ": " + editTextVO.getEditText().getText();
                if (!editTextVO.isLast()) {
                    j++;
                    EditTextVO editTextVO1 = editTexts.get(j);
                    potInformation = potInformation + ",\n\t\t" + editTextVO1.getEditText().getTag() + ": " + editTextVO1.getEditText().getText();
                }
                potInformation = potInformation + "\n\n";
               /* if (potVO.getColorId() == -1) {
                    potInformation = potInformation + "Item name: " + potVO.getItemName() + ",   Default Color " + ",   Size: " + editText.getText().toString() + "\n";
                } else {
                    if (potVO.getColorName() != null) {
                        potInformation = potInformation + "Item name: " + potVO.getItemName() + ",   " + potVO.getColorName() + ": " + potVO.getColorCode() + ",   Size: " + editText.getText().toString() + "\n";
                    } else {
                        potInformation = potInformation + "Item name: " + potVO.getItemName() + ",   Default Color " + ",   Size: " + editText.getText().toString() + "\n";
                    }
                }*/
            }
            return potInformation;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        editor.putBoolean("isActivityPaused", true);
        editor.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sharedPreferences.getBoolean("isActivityPaused", false) && sharedPreferences.getBoolean("isEmailBtnClicked", false)) {

            Intent intent = new Intent(PreviewActivity.this, CreateArtActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("from_preview", true);
            intent.putExtra(Constants.IS_NEED_TO_INSERT,false);
            intent.putExtras(bundle);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    private void sentEmailUsingIntent(String fileName) {

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setClassName(selectedEmailClientPackageName, selectedEmailClientComposeActivity);
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fileName)));
     /*   if (!comment.getText().toString().isEmpty()) {
            emailIntent.putExtra(Intent.EXTRA_TEXT, comment.getText().toString());
        }*/
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{toEmailId.getText().toString()});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, Constants.DEFAULT_SUBJECT);
        editor.putBoolean("isEmailBtnClicked", true);
        editor.commit();
        emailIntent.setType("application/pdf");
        startActivity(emailIntent);
    }

    private HashMap getPotInfoForPDF() {
        try {
            categoryItemMap = new HashMap<String,ArrayList<String>>(0);
            String potInformation = "";
            int id;
            int parentId;
            String name;
           // Toast.makeText(getApplicationContext(),"Preview PotList Size :::" + potList.size(),Toast.LENGTH_SHORT).show();
           // Toast.makeText(getApplicationContext(),"Preview EditText Size :::" + editTexts.size(),Toast.LENGTH_SHORT).show();

            for (int i = 0, j = 0; i < potList.size() && j < editTexts.size(); i++, j++) {
                ItemVO potVO = potList.get(i);
                parentId = potVO.getParentId();
                id = CatalogueJsonParser.getTopParent(this, potVO);
                name = CatalogueJsonParser.getCategoryName();
                EditTextVO editTextVO = editTexts.get(j);
                potInformation = potInformation +   " Item name: " + potVO.getItemName() +
                        ",  " + editTextVO.getEditText().getTag() + ": " + editTextVO.getEditText().getText();
                if (!editTextVO.isLast()) {
                    j++;
                    EditTextVO editTextVO1 = editTexts.get(j);
                    potInformation = potInformation + ",  " + editTextVO1.getEditText().getTag() + ": " + editTextVO1.getEditText().getText();
                }

                if(categoryItemMap.size() > 0){
                    iteamList = categoryItemMap.get(name);
                    if(iteamList == null){
                        iteamList = new ArrayList<>(0);
                    }
                }
                iteamList.add(potInformation);
                potInformation = "";
                categoryItemMap.put(name,iteamList);
               // potInformation = potInformation + ",,";
            }
            return categoryItemMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onMediaScannerConnected() {
        try {
            mediaScannerConnection.scanFile(artVO.getEditedImagePath(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {

    }

    IOnUserRecoverableException iOnUserRecoverableException = new IOnUserRecoverableException() {
        @Override
        public void onUserRecoverableException(UserRecoverableAuthException exception) {
            // this app to have permission to read my spreadsheet."
            Intent recoveryIntent = exception.getIntent();
            startActivityForResult(recoveryIntent, RECOVERY_REQUEST_CODE);
        }
    };


    private Handler authHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if(msg != null){
                Bundle bundle = msg.getData();
                if(bundle != null){
                    String message = bundle.getString("message");
                    if(message != null){
                        Toast.makeText(PreviewActivity.this,message,Toast.LENGTH_SHORT).show();
                    }
                }

            }


        }
    };
}
