package com.spec.ambius.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;

import com.spec.ambius.R;
import com.spec.ambius.common.adapter.GalleryAdapter;
import com.spec.ambius.common.customui.EmptyRecyclerView;
import com.spec.ambius.common.database.DatabaseManager;
import com.spec.ambius.vo.ArtVO;

import java.util.ArrayList;
import java.util.Collections;

public class GalleryActivity extends TitleActivity {

    private EmptyRecyclerView gridView;
    private DatabaseManager mDatabaseManager;
    private GalleryAdapter mAdapter;
    private ArrayList<ArtVO> mArtVOs = new ArrayList<ArtVO>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView(R.layout.activity_gallery);
        setActionBarIcon(R.drawable.ic_launcher);

        mDatabaseManager = new DatabaseManager(this);
        gridView = (EmptyRecyclerView) findViewById(R.id.galleryView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 5);
        gridView.setLayoutManager(gridLayoutManager);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_gallery;
    }

    @Override
    protected void onResume() {
        if (mAdapter == null) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    mArtVOs = mDatabaseManager.getAllArts();
                    Collections.reverse(mArtVOs);
                    Message message = new Message();
                    message.obj = mArtVOs;
                    handler.sendMessage(message);
                }
            });
            thread.start();
        }
        super.onResume();
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mAdapter = new GalleryAdapter(GalleryActivity.this, (ArrayList<ArtVO>) msg.obj);
            gridView.setAdapter(mAdapter);
        }
    };

}
