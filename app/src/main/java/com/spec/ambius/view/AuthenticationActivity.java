package com.spec.ambius.view;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.spec.ambius.R;
import com.spec.ambius.common.callbackinterface.IOnUserRecoverableException;
import com.spec.ambius.common.constants.Constants;
import com.spec.ambius.common.util.GoogleSheetUtility;
import com.spec.ambius.common.util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class AuthenticationActivity extends TitleActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Button btnAuthenticateGoogle;
    private Button btnAddNewEmailAddress;
    private Spinner spinnerEmailAddress;
  //  private EditText edtTextEmailId;

    //Progessbar that we show to user while connecting GoogleApiClient instance
    private ProgressDialog progressDialog;

    //LOG_TAG is used for printing logs
    private static final String LOG_TAG = MainActivity.class.getSimpleName();


    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    //AccountManager instance to perform account related operations
    private AccountManager accountManager;

    //emailId that we get from user
    private String emailId;

    private ArrayList<String> domainList;
    private ArrayList<String> emailAddresses;
    private Account account;

    private ArrayAdapter<String> emailAddressAdapter;
    private static final int RECOVERY_REQUEST_CODE = 8888;

    //AccountManagerFuture bundle that we get as a result of AccountManager.addAccount method
    private AccountManagerFuture<Bundle> futureBundle;
    //Handler is used to retrieve getResult from AccountManagerFuture instance as this method blocks UI thread we call it on
    //another thread
    private Handler handler = new Handler();

    private GoogleSheetUtility googleSheetUtility;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_launcher);

        //edtTextEmailId = (EditText) findViewById(R.id.edtTextEmailId);
        btnAuthenticateGoogle = (Button) findViewById(R.id.btnAuthenticateGoogle);
        btnAddNewEmailAddress = (Button) findViewById(R.id.btnAddNewEmailAddress);
        spinnerEmailAddress = (Spinner) findViewById(R.id.spinnerEmailAddress);

       /* edtTextEmailId.requestFocus();
        edtTextEmailId.setFocusable(true);*/
        btnAuthenticateGoogle.setOnClickListener(this);
        btnAddNewEmailAddress.setOnClickListener(this);
        domainList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.account_domain)));
        emailAddresses = new ArrayList<String>(0);
        emailAddresses.add(Constants.SELECT_EMAIL_ID);
        accountManager = AccountManager.get(this);
        getAccountAlreadyConfigured();
        emailAddressAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, emailAddresses);
        spinnerEmailAddress.setAdapter(emailAddressAdapter);
        spinnerEmailAddress.setOnItemSelectedListener(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_authentication;
    }


    @Override
    public void onClick(View view) {
        if (view == btnAuthenticateGoogle) {

            //We check for internet connectivity before we proceed further
            if (Util.getConnectivityStatusString(this).equals(Constants.TYPE_NOT_CONNECTED_STRING)) {
                Util.showToast(AuthenticationActivity.this, getResources().getString(R.string.msg_internet_connection));
                return;
            }


            // emailId = edtTextEmailId.getText().toString();
            if(emailAddresses.size() > 1){
                if (emailId != null && !(emailId.equalsIgnoreCase(Constants.SELECT_EMAIL_ID))) {
                    accountManager = AccountManager.get(this);

                    if (isEmailValid(emailId)) {


                        //Check whether account is already configured or not
                        if (isAccountAlreadyConfigured(emailId)) {
                       /*     getGoogleApiClientInstance(emailId);
                            connectToGoogleApiClient();*/
                            googleSheetUtility = new GoogleSheetUtility(iOnUserRecoverableException, getString(R.string.app_name_google_console), Constants.SPREADSHEET_FEED_URL, AuthenticationActivity.this);
                            if (emailId != null && !emailId.equals("")){
                                googleSheetUtility.authenticateUser(emailId);
                            }else {
                                /*Utility.showSnackBar(linearLayout, Constants.ACCOUNT_NAME_NULL_OR_EMPTY);*/
                                Toast.makeText(AuthenticationActivity.this,Constants.ACCOUNT_NAME_NULL_OR_EMPTY,Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Bundle accountOptions = new Bundle();
                            accountOptions.putString(AccountManager.KEY_ACCOUNT_NAME, emailId);
                            accountOptions.putString(AccountManager.KEY_ACCOUNT_TYPE, Constants.ACCOUNT_TYPE);
                            accountManager.addAccount(Constants.ACCOUNT_TYPE, null, null, accountOptions, this, accountManagerCallback, null);
                        }
                    }
                } else {
                    Util.showToast(AuthenticationActivity.this, Constants.EMPTY_EMAIL_ID_ERROR);
                }
            }else{
                Util.showToast(AuthenticationActivity.this, Constants.ADD_EMAIL_ID);
            }

            //edtTextEmailId.setText("");
        }else if(view == btnAddNewEmailAddress){

            if (Util.getConnectivityStatusString(this).equals(Constants.TYPE_NOT_CONNECTED_STRING)) {
                Util.showToast(AuthenticationActivity.this, getResources().getString(R.string.msg_internet_connection));
                return;
            }
            Bundle accountOptions = new Bundle();
            accountOptions.putString(AccountManager.KEY_ACCOUNT_NAME, Constants.BLANK_STRING);
            accountOptions.putString(AccountManager.KEY_ACCOUNT_TYPE, Constants.ACCOUNT_TYPE);
            accountManager.addAccount(Constants.ACCOUNT_TYPE, null, null, accountOptions, this, accountManagerCallback, null);
        }
    }

    /* @method getGoogleApiClientInstance is used to get GoogleApiClientInstance for a particular google account
     * @Param String : gmail account Id
	 * @return GoogleApiClient instance
	 * */
//    private GoogleApiClient getGoogleApiClientInstance(String accountName) {
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(Plus.API)
//                .addScope(Plus.SCOPE_PLUS_LOGIN)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .setAccountName(accountName)
//                .build();
//
//        return mGoogleApiClient;
//    }

    /* @method isAccountAlreadyConfigured is used to check whether the email address entered by user is already
     * configured in the device or not
     * @param String: it takes the email address as the parameter
     * @return boolean*/
    private boolean isAccountAlreadyConfigured(String accountName) {
        Account[] accounts = accountManager.getAccounts();
        for (Account account : accounts) {
            if (account.name.equals(accountName)) {
                this.account = account;
                return true;
            }
            Log.i(LOG_TAG, "Account info: " + account.name + "\n" + "Account type: " + account.type);
        }
        return false;
    }

    private void getAccountAlreadyConfigured() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(Constants.PROGRESS_DIALOG_LOADING);
        progressDialog.show();
        boolean isValid = false;
        Account[] accounts = accountManager.getAccounts();
        for (Account account : accounts) {
            isValid = false;
            for (String domain : domainList) {
                if (((String) account.name).endsWith("@" + domain)) {
                    isValid = true;
                    break;
                } else {
                    isValid = false;
                }

            }
            if(isValid){
                emailAddresses.add(account.name);
                //Toast.makeText(getApplicationContext(),"Email Address ::" + account.name,Toast.LENGTH_SHORT).show();
            }
            Log.i(LOG_TAG, "Account info: " + account.name + "\n" + "Account type: " + account.type);
        }
        Collections.sort(emailAddresses);
      dismissProgressDialog();
    }

//    @Override
//    public void onConnected(Bundle bundle) {
//        Log.i(LOG_TAG, "onConnected");
//        // We've resolved any connection errors.  mGoogleApiClient can be used to
//        // access Google APIs on behalf of the user.
//       /* if(account == null) {
//            isAccountAlreadyConfigured(emailId);
//        }
//        accountManager.confirmCredentials(account,bundle,this,signInAccountManagerCallback,signInHandler);
//*/
//        dismissProgressDialog();
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            if (Plus.AccountApi.getAccountName(mGoogleApiClient).contains(emailId)) {
//                Toast.makeText(this, "Signed in successfully", Toast.LENGTH_SHORT).show();
//            }
//            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
//                Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//                String displayName = person.getDisplayName();
//                String userId = person.getId();
//
//                Intent authenticateResultActivityIntent = new Intent(AuthenticationActivity.this, CreateArtActivity.class);
//                authenticateResultActivityIntent.putExtra(Constants.DISPLAY_NAME, displayName);
//                authenticateResultActivityIntent.putExtra(Constants.USER_ID, userId);
//                startActivity(authenticateResultActivityIntent);
////						Util.showToast(MainActivity.this, "Display Name: "+displayName + "\n" + "User Id: "+userId);
//
//            }
//        } else {
//            progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage(Constants.PROGRESS_DIALOG_MESSAGE);
//            progressDialog.show();
//            mGoogleApiClient.connect();
//        }
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//        Log.d(LOG_TAG, "onConnectionSuspended");
//        dismissProgressDialog();
//        showAlertDialog(Constants.CONNECTION_SUSPENDED);
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//        Log.d(LOG_TAG, "onConnectionFailed");
//        dismissProgressDialog();
//        // showAlertDialog(Constants.CONNECTION_FAILED);
//
//        if (connectionResult.hasResolution()) {
//            try {
//                connectionResult.startResolutionForResult(this, connectionResult.getErrorCode());
//            } catch (IntentSender.SendIntentException e) {
//                mGoogleApiClient.connect();
//            }
//        }
//    }

    /* isEmailId method is used to validate entered email address. Apart from regex validation and default Pattern
    Email Address validation we have used "gmail" validation particularly as we are aiming for google plus validation
	so we check for "@gmail.com" as a suffix

	@Param CharSequence
	@Param String
	@return boolean	*/
    private boolean isEmailValid(CharSequence emailId) {
        boolean isValid = false;
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches() && Constants.EMAIL_ADDRESS_PATTERN.matcher(emailId).matches()) {
            for (String domain : domainList) {
                if (((String) emailId).endsWith("@" + domain)) {
                    isValid = true;
                    break;
                } else {
                    isValid = false;
                }
            }
            if (isValid) {
                return true;
            } else {
                Util.showToast(AuthenticationActivity.this, Constants.INVALID_DOMAIN_ERROR);
            }

        } else {
            Util.showToast(AuthenticationActivity.this, Constants.INVALID_EMAIL_ID);
        }
        return false;
    }


   /* private final Handler signInHandler = new Handler() {

        public void handleMessage(Message msg) {
           if(msg.what == 0){
               Toast.makeText(getApplicationContext(),"SignIn Successfully!",Toast.LENGTH_LONG).show();
           }else if(msg.what == 1){
               Toast.makeText(getApplicationContext(),"Invalid Password!",Toast.LENGTH_LONG).show();
           }
        }
    };
*/

  /*  private AccountManagerCallback signInAccountManagerCallback = new AccountManagerCallback() {
        @Override
        public void run(AccountManagerFuture accountManagerFuture) {
            try {

                Bundle response = (Bundle) accountManagerFuture.getResult();
               boolean val =  response.getBoolean("confirmResult");
                String msg = response.toString();
               System.out.println("Response ::::::" + msg);

                if (val) {
                    if (Plus.AccountApi.getAccountName(mGoogleApiClient).contains(emailId)) {
                        //  Toast.makeText(this, "Signed in successfully", Toast.LENGTH_SHORT).show();
                    }
                    if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                        Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                        String displayName = person.getDisplayName();
                        String userId = person.getId();

                        Intent authenticateResultActivityIntent = new Intent(GoogleAuthenticationActivity.this, CreateArtActivity.class);
                        authenticateResultActivityIntent.putExtra(Constants.DISPLAY_NAME, displayName);
                        authenticateResultActivityIntent.putExtra(Constants.USER_ID, userId);
                        startActivity(authenticateResultActivityIntent);

                        signInHandler.sendEmptyMessage(0);

//						Util.showToast(MainActivity.this, "Display Name: "+displayName + "\n" + "User Id: "+userId);

                    }
                } else {
                    signInHandler.sendEmptyMessage(1);
                }

            } catch (OperationCanceledException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (AuthenticatorException e) {
                e.printStackTrace();
            }



        }
    };
*/

    /* accountManagerCallback is used to pass as one of  the params to AccountManager.addAccount method*/
    private AccountManagerCallback<Bundle> accountManagerCallback = new AccountManagerCallback<Bundle>() {

        @Override
        public void run(AccountManagerFuture<Bundle> future) {
            futureBundle = future;

            Log.i(LOG_TAG, "Done: " + future.isDone() + "\n" + "isCancelled: " + future.isCancelled());
            handler.post(runnable);
        }
    };

    private Runnable runnable = new Runnable() {

        @Override
        public void run() {

            try {
                // futureBundle.getResult() call will block until the result is available so we call it on another thread
                Bundle bundle = futureBundle.getResult();
                if (bundle != null) {
                    Log.i(LOG_TAG, "Result: " + bundle);
                    String authAccount = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
                    String accountType = bundle.getString(AccountManager.KEY_ACCOUNT_TYPE);
                    emailId = authAccount;
                    if(emailId  != null){
                        if(emailAddresses != null && !emailAddresses.contains(emailId)){
                            emailAddresses.add(emailId);
                            Collections.sort(emailAddresses);
                        }
                        if (Util.getConnectivityStatusString(AuthenticationActivity.this).equals(Constants.TYPE_NOT_CONNECTED_STRING)) {
                            Util.showToast(AuthenticationActivity.this, getResources().getString(R.string.msg_internet_connection));
                        }else{
                            if ((authAccount.equals(emailId) && accountType.equals(Constants.ACCOUNT_TYPE)) || isAccountAlreadyConfigured(emailId)) {
                                googleSheetUtility = new GoogleSheetUtility(iOnUserRecoverableException, getString(R.string.app_name_google_console), Constants.SPREADSHEET_FEED_URL, AuthenticationActivity.this);
                                if (emailId != null && !emailId.equals("")){
                                    googleSheetUtility.authenticateUser(emailId);
                                }else {
                                /*Utility.showSnackBar(linearLayout, Constants.ACCOUNT_NAME_NULL_OR_EMPTY);*/
                                    Toast.makeText(AuthenticationActivity.this,Constants.ACCOUNT_NAME_NULL_OR_EMPTY,Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Util.showToast(AuthenticationActivity.this, Constants.ERROR_WHILE_CONFIGURING);
                            }

                        }
                    }


                }
            } catch (OperationCanceledException e) {
                e.printStackTrace();
            } catch (AuthenticatorException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    };


    /* @method connectToGoogleApiClient is used to connect GoogleApiClient instance to Google API servers */
    private void connectToGoogleApiClient() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(Constants.PROGRESS_DIALOG_MESSAGE);
        progressDialog.show();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            showAlertDialog(Constants.CONNECTION_FAILED);
        }
    }

    /* @method dismissProgressDialog is used to dismiss progress dialog */
    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*@method showAlertDialog is used to ask user that the connection has been suspended or failed due to
     * service connection issue so do they want to retry or not */
    private void showAlertDialog(String message) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AuthenticationActivity.this)
                .setMessage(message)
                .setNegativeButton(Constants.CANCEL, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton(Constants.OK, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mGoogleApiClient != null) {
                            mGoogleApiClient.disconnect();
                        }
//                        getGoogleApiClientInstance(emailId);
                        connectToGoogleApiClient();
                    }
                });
        alertDialogBuilder.create();
        alertDialogBuilder.show();

    }

    @Override
    protected void onResume() {

       /* final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        edtTextEmailId.postDelayed(new Runnable() {
            @Override
            public void run() {
                edtTextEmailId.requestFocus();
                inputMethodManager.showSoftInput(edtTextEmailId, 0);
            }
        }, 100);*/
        super.onResume();

        spinnerEmailAddress.setSelection(0);
        emailId = emailAddresses.get(0);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
       // Toast.makeText(AuthenticationActivity.this, "Selected Email::" + emailAddresses.get(i), Toast.LENGTH_SHORT).show();
            emailId = emailAddresses.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    IOnUserRecoverableException iOnUserRecoverableException = new IOnUserRecoverableException() {
        @Override
        public void onUserRecoverableException(UserRecoverableAuthException exception) {
            // this app to have permission to read my spreadsheet."
            Intent recoveryIntent = exception.getIntent();
            startActivityForResult(recoveryIntent, RECOVERY_REQUEST_CODE);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RECOVERY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null && !accountName.equals("")) {
                        googleSheetUtility.authenticateUser(emailId);
                    } else {
                        Toast.makeText(AuthenticationActivity.this, Constants.ACCOUNT_NAME_NULL_OR_EMPTY, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(AuthenticationActivity.this, Constants.INTENT_DATA_NULL, Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(AuthenticationActivity.this, Constants.CHOOSER_CANCELLED, Toast.LENGTH_SHORT).show();

            }
        }
    }

    private Handler authHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(msg != null){
                Bundle bundle = msg.getData();
                if(bundle != null){
                    String message = bundle.getString("message");
                    if(message != null){
                        Toast.makeText(AuthenticationActivity.this,message,Toast.LENGTH_SHORT).show();
                    }
                }

            }


        }
    };








}
