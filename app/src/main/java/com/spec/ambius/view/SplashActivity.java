package com.spec.ambius.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.spec.ambius.R;
import com.spec.ambius.common.constants.Constants;


public class SplashActivity extends TitleActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView();
        setActionBarIcon(R.drawable.ic_launcher);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences prefs = getSharedPreferences(Constants.MyPREFERENCES, MODE_PRIVATE);
                String emailAddress = prefs.getString(Constants.USER_EMAIL_PREF, null);
                if(emailAddress != null && !emailAddress.equals("")){
                    Intent intent = new Intent(SplashActivity.this,CreateArtActivity.class);
                    intent.putExtra(Constants.IS_NEED_TO_INSERT,true);
                    startActivity(intent);
                }else{
                    startActivity(new Intent(SplashActivity.this,AuthenticationActivity.class));
                }

                finish();
            }
        }, 2000);

    }
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }
}
