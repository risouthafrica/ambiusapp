package com.spec.ambius.view;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.spec.ambius.R;
import com.spec.ambius.common.adapter.CustomArrayAdapter;
import com.spec.ambius.common.adapter.PotsAdapter;
import com.spec.ambius.common.constants.Constants;
import com.spec.ambius.common.constants.Global;
import com.spec.ambius.common.customui.HorizontalListView;
import com.spec.ambius.common.database.DatabaseManager;
import com.spec.ambius.common.imageutility.CollageView;
import com.spec.ambius.common.imageutility.MultiTouchListener;
import com.spec.ambius.common.imageutility.RootViewMultiTouchListener;
import com.spec.ambius.common.parser.CatalogueJsonParser;
import com.spec.ambius.common.util.OnSingleTapListener;
import com.spec.ambius.common.util.SaveImageUtil;
import com.spec.ambius.common.util.Util;
import com.spec.ambius.vo.ArtVO;
import com.spec.ambius.vo.ColorVO;
import com.spec.ambius.vo.ItemVO;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends TitleActivity implements OnClickListener, AdapterView.OnItemClickListener,
        MediaScannerConnection.MediaScannerConnectionClient, AdapterView.OnItemSelectedListener {

    private String TAG = "MainActivity";

    private ImageView imgView, imgViewForeground;
    private RelativeLayout rltRootView;
    private RelativeLayout artLayout;
    private Bitmap grayScaledBitmap;
    private Bitmap fgTransparent;
    private ProgressDialog progressDialog;

    private ImageButton btnClose;
    private ImageView imgBackground;
    private ArrayList<ColorVO> colorVOs;

    private RelativeLayout layoutOptions;
    private ImageButton toggleButton;

    private HorizontalListView horizontalListViewColor;
    private HorizontalListView horizontalListViewPots;
    private CustomArrayAdapter customArrayAdapter;
    private PotsAdapter potsAdapter;
    private ArrayList<Integer> Ids = new ArrayList<>();
    private ArtVO artVO;
    private boolean isItemSelection = false;

    private Bitmap originalBitmap;
    private ArrayList<ItemVO> potVOArrayList;
    private ArtVO mArtVO;
    private String filePath;
    private HashMap<Integer, ItemVO> potMap;
    private boolean isColorAvailable = false;
    private int sdk = android.os.Build.VERSION.SDK_INT;
    private Resources resources;
    private ActionBarDrawerToggle mDrawerToggle;
    //Expandable list view
    private Spinner firstCategorySpinner, secondCategorySpinner, thirdCategorySpinner, fourthCatagorySpinner;
    private ArrayList<ItemVO> firstCategoryList, secondCategoryList, thirdCategoryList, fourthItemList, itemVOList;
    private ArrayAdapter<ItemVO> firstCategoryAdapter, secondCategoryAdapter, thirdCategoryAdapter, fourthCategoryAdapter;
    private DrawerLayout drawerLayout;

    private Button btnBack;
    private boolean isLongPressed = false;

    private Set<CollageView> iteamGroupSet = new HashSet(0);
    private static MultiTouchListener multiTouchListener;
    private MenuItem menuUngroup;
    private MenuItem menuDeleteGroup;


    @Override
    public void onPanelClosed(int featureId, Menu menu) {
        super.onPanelClosed(featureId, menu);
    }

    private int currentSpinner = Constants.DEFAULT_SPINNER;

    private MediaScannerConnection mediaScannerConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_main);
        setActionBarIcon(R.drawable.ic_launcher);
        init();
        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {

                if (extras.containsKey("filePath")) {
                    filePath = getIntent().getStringExtra("filePath");
                    if (filePath != null) {

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        originalBitmap = BitmapFactory.decodeFile(filePath, options);
                        //imgBackground.setImageBitmap(originalBitmap);
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            artLayout.setBackgroundDrawable(new BitmapDrawable(originalBitmap));
                        } else {
                            artLayout.setBackground(new BitmapDrawable(getResources(), originalBitmap));
                        }

                    }
                } else {
                    mArtVO = extras.getParcelable("artVO");
                    //imgBackground.setImageBitmap(ImageLoader.loadFromUri(this, mArtVO.getEditedImagePath(), 4096, 4096));
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    originalBitmap = BitmapFactory.decodeFile(mArtVO.getEditedImagePath(), options);
                    //imgBackground.setImageBitmap(originalBitmap);
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        artLayout.setBackgroundDrawable(new BitmapDrawable(originalBitmap));
                    } else {
                        artLayout.setBackground(new BitmapDrawable(getResources(), originalBitmap));
                    }
                }
            }
            if (getIntent().getBooleanExtra("isCaptured", false)) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mediaScannerConnection.connect();
                    }
                }).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        itemVOList = new ArrayList<>();
        CatalogueJsonParser.getAllItemsFromJson(this, itemVOList);
        Log.d(TAG, "Total size : " + itemVOList.size());
        initCategories();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    private void init() {

        mediaScannerConnection = new MediaScannerConnection(this, this);
        resources = getResources();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        imgBackground = (ImageView) findViewById(R.id.imgBackground);
        rltRootView = (RelativeLayout) findViewById(R.id.rootView);
        rltRootView.setOnClickListener(this);
        artLayout = (RelativeLayout) findViewById(R.id.artLayout);

        layoutOptions = (RelativeLayout) findViewById(R.id.layoutOptions);

        btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        toggleButton = (ImageButton) findViewById(R.id.toggle_button);
        toggleButton.setOnClickListener(this);
        isItemSelection = false;

        horizontalListViewColor = (HorizontalListView) findViewById(R.id.lstColors);
        horizontalListViewColor.setOnItemClickListener(this);
        colorVOs = new ArrayList<ColorVO>();
        CatalogueJsonParser.getAllColors(colorVOs, this);
        customArrayAdapter = new CustomArrayAdapter(this, colorVOs);
        horizontalListViewColor.setAdapter(customArrayAdapter);
        Global.isGroupCreated = false;
        potMap = new HashMap<Integer, ItemVO>(0);
//        getTempJson();

        horizontalListViewPots = (HorizontalListView) findViewById(R.id.lstPots);
        horizontalListViewPots.setOnItemClickListener(this);
        potVOArrayList = new ArrayList<>();
        potsAdapter = new PotsAdapter(this, potVOArrayList);
        horizontalListViewPots.setAdapter(potsAdapter);

        rltRootView.setOnTouchListener(new RootViewMultiTouchListener(MainActivity.this) {
            @Override
            public void onSwipeBottom() {
                if (layoutOptions.getVisibility() == View.GONE && !Global.isGroupCreated) {
                    handleBottomUI(true);
                }
            }

            @Override
            public void onSwipeCloseBottom() {
                if (layoutOptions.getVisibility() == View.VISIBLE && !Global.isGroupCreated) {
                    handleBottomUI(false);
                }
            }

            @Override
            public void onSingleTapUP() {
                if (!Global.isGroupCreated) {
                    if (layoutOptions.getVisibility() == View.GONE) {
                        //imgColorPalette.setVisibility(View.INVISIBLE);
                        handleBottomUI(true);
                        horizontalListViewColor.setVisibility(View.GONE);
                        horizontalListViewPots.setVisibility(View.VISIBLE);
                        toggleButton.setImageResource(R.drawable.ic_action_image_color_lens);
                        toggleButton.setVisibility(View.VISIBLE);
                        isItemSelection = false;
                    } else {
                        handleBottomUI(false);
                    }

                    if (btnClose != null && btnClose.getVisibility() == View.VISIBLE)
                        btnClose.setVisibility(View.GONE);

                    if (Global.targetView != null) {
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            Global.targetView.setBackgroundResource(android.R.color.transparent);
                        } else {
                            Global.targetView.setBackground(null);
                        }
                        Global.targetView = null;
                        imgView = null;
                        imgViewForeground = null;
                    }
                } else {
                    layoutOptions.setVisibility(View.GONE);
                }

            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, 0, 0) {
            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(mDrawerToggle);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        drawerLayout.openDrawer(GravityCompat.START);
        if (!Global.isGroupCreated) {
            handleBottomUI(false);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void handleBottomUI(boolean show) {
        if (show) {
            slideShowBar(R.anim.slide_editor_bar_open_port, layoutOptions);
        } else {
            slideHideBar(R.anim.slide_editor_bar_close_port, layoutOptions);
          /*  if (horizontalListViewColor.getVisibility() == View.VISIBLE)
                slideHideBar(R.anim.slide_editor_bar_close_port, horizontalListViewColor);
            if (horizontalListViewPots.getVisibility() == View.VISIBLE)
                slideHideBar(R.anim.slide_editor_bar_close_port, horizontalListViewPots);*/
            /*if (view.getVisibility() == View.VISIBLE)
                slideHideBar(R.anim.slide_editor_bar_close_port, view);*/
        }
    }

    private OnSingleTapListener singleTapListener = new OnSingleTapListener() {
    /*    @Override
        public String toString() {
            return super.toString();
        }*/

        @Override
        public void onLongPress(boolean isVisible) {

            isLongPressed = true;
            if (Global.targetView != null && !iteamGroupSet.contains(Global.targetView) && !Global.isGroupCreated) {
                iteamGroupSet.add(Global.targetView);
            }

            btnClose = (ImageButton) Global.targetView.findViewById(R.id.btnClose);
            if (isVisible) {
                btnClose.setVisibility(View.VISIBLE);
            } else {
                btnClose.setVisibility(View.GONE);
            }
        }

        @Override
        public void onSingleTap(boolean isVisible) {
            if ((isLongPressed || Global.isGroupCreated) && Global.targetView != null) {
                if(!iteamGroupSet.contains(Global.targetView)){
                    Global.isGroupCreated = true;
                    iteamGroupSet.add(Global.targetView);
                    isLongPressed = false;
                    horizontalListViewColor.setVisibility(View.GONE);
                    horizontalListViewPots.setVisibility(View.GONE);
                    layoutOptions.setVisibility(View.GONE);
                    Global.targetView.setOnTouchListener(multiTouchListener);
                    menuUngroup.setVisible(true);
                    menuDeleteGroup.setVisible(true);
                    for (CollageView collageView : iteamGroupSet) {

                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            collageView.setBackgroundResource(R.drawable.border);
                        } else {
                            collageView.setBackground(getResources().getDrawable(R.drawable.border));
                        }
                    }
                }
            }
            //  Toast.makeText(getApplicationContext(),"size" + iteamGroupSet.size(),Toast.LENGTH_LONG).show();
            if (!Global.isGroupCreated) {
                layoutOptions.setVisibility(View.VISIBLE);
                if (isVisible) {
                    isColorAvailable = true;
                    horizontalListViewColor.setVisibility(View.VISIBLE);
                    horizontalListViewPots.setVisibility(View.GONE);
                    toggleButton.setImageResource(R.drawable.ic_action_notification_adb);
                    toggleButton.setVisibility(View.VISIBLE);
                    isItemSelection = true;
                } else {
                    isColorAvailable = false;
                    horizontalListViewColor.setVisibility(View.GONE);
                    horizontalListViewPots.setVisibility(View.VISIBLE);
                    toggleButton.setImageResource(R.drawable.ic_action_image_color_lens);
                    toggleButton.setVisibility(View.VISIBLE);
                    isItemSelection = false;
                }
                if (layoutOptions.getVisibility() == View.GONE) {
                    handleBottomUI(true);
                }
            } else {
                if(iteamGroupSet.size() > 1){
                    btnClose.setVisibility(View.GONE);
                  layoutOptions.setVisibility(View.GONE);
                }else{
                    btnClose.setVisibility(View.VISIBLE);
                   layoutOptions.setVisibility(View.VISIBLE);
            }

            }

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClose:
                horizontalListViewColor.setVisibility(View.GONE);
                horizontalListViewPots.setVisibility(View.VISIBLE);
                toggleButton.setImageResource(R.drawable.ic_action_image_color_lens);
                toggleButton.setVisibility(View.VISIBLE);
                isItemSelection = false;

                artLayout.removeView(Global.targetView);
                if (potMap.containsKey(Global.targetView.getId())) {
                    potMap.remove(Global.targetView.getId());
                }
               // Toast.makeText(getApplicationContext(),"After Delete Size :::" + potMap.size(),Toast.LENGTH_SHORT).show();
                Global.targetView = null;
                imgView = null;
                imgViewForeground = null;
                isLongPressed = false;
                Global.isGroupCreated = false;
                iteamGroupSet.clear();
                break;
            case R.id.rootView:
                if (layoutOptions.getVisibility() == View.VISIBLE) {
                    slideHideBar(R.anim.slide_editor_bar_close_port, layoutOptions);
                }
                horizontalListViewColor.setVisibility(View.GONE);
                horizontalListViewPots.setVisibility(View.GONE);
                //view.setVisibility(View.GONE);
                if (btnClose != null && btnClose.getVisibility() == View.VISIBLE)
                    btnClose.setVisibility(View.GONE);
                /*if (Global.targetView != null) {
                    Global.targetView.setBackground(null);
                    Global.targetView = null;
                    imgView = null;
                    imgViewForeground = null;
                }*/
                break;

            case R.id.toggle_button:
                if (isItemSelection) {
                    horizontalListViewColor.setVisibility(View.GONE);
                    horizontalListViewPots.setVisibility(View.VISIBLE);
                    //toggleButton.setVisibility(View.GONE);
                    toggleButton.setImageResource(R.drawable.ic_action_image_color_lens);
                    isItemSelection = false;
                } else {
                    if (isColorAvailable) {
                        if (Global.targetView == null) {
                            Toast.makeText(this, resources.getString(R.string.select_item), Toast.LENGTH_SHORT).show();
                        }
                        horizontalListViewPots.setVisibility(View.GONE);
                        horizontalListViewColor.setVisibility(View.VISIBLE);
                        //toggleButton.setVisibility(View.GONE);
                        toggleButton.setImageResource(R.drawable.ic_action_notification_adb);
                        isItemSelection = true;
                    } else {
                        Toast.makeText(MainActivity.this, resources.getString(R.string.no_color), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.btnBack:
                if (currentSpinner == Constants.THIRD_SPINNER && fourthItemList != null) {
                    currentSpinner = Constants.SECOND_SPINNER;
                    fourthItemList.clear();
//                    if(fourthCatagorySpinner.isShown()){
//                        fourthCatagorySpinner.performClick();
//                    }
                    fourthCatagorySpinner.setSelection(-1);
                    fourthCatagorySpinner.setVisibility(View.GONE);
                    thirdCategorySpinner.setSelection(0);
                    thirdCategorySpinner.performClick();
                } else if (currentSpinner == Constants.SECOND_SPINNER && thirdCategoryList != null) {
                    currentSpinner = Constants.FIRST_SPINNER;
                    thirdCategoryList.clear();
//                    if(thirdCategorySpinner.isShown()){
//                        thirdCategorySpinner.performClick();
//                    }

                    thirdCategorySpinner.setSelection(-1);
                    thirdCategorySpinner.setVisibility(View.GONE);
                    secondCategorySpinner.setSelection(0);
                    secondCategorySpinner.performClick();
                } else if (currentSpinner == Constants.FIRST_SPINNER && secondCategoryList != null) {
                    currentSpinner = Constants.DEFAULT_SPINNER;
                    secondCategoryList.clear();
//                    if(secondCategorySpinner.isShown()){
//                        secondCategorySpinner.performClick();
//                    }
                    secondCategorySpinner.setSelection(-1);
                    secondCategorySpinner.setVisibility(View.GONE);
                    firstCategorySpinner.setSelection(0);
                    firstCategorySpinner.performClick();
                } else if (firstCategorySpinner != null) {
                    firstCategorySpinner.setSelection(0);
                    firstCategorySpinner.performClick();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menuUngroup = menu.findItem(R.id.action_ungroup);
        menuDeleteGroup = menu.findItem(R.id.action_delete_group);
        if(!Global.isGroupCreated){
            menuUngroup.setVisible(false);
            menuDeleteGroup.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            //return true;
            if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        }
        switch (id) {
            case R.id.action_preview:
                if (Global.targetView != null) {
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        Global.targetView.setBackgroundResource(android.R.color.transparent);
                    } else {
                        Global.targetView.setBackground(null);
                    }
                }

                if (iteamGroupSet.size() > 0 && Global.isGroupCreated) {
                    for (CollageView collageView : iteamGroupSet) {
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            collageView.setBackgroundResource(android.R.color.transparent);
                        } else {
                            collageView.setBackground(null);
                        }
                    }
                }

                if (btnClose != null && btnClose.getVisibility() == View.VISIBLE) {
                    btnClose.setVisibility(View.GONE);
                }
               // Toast.makeText(getApplicationContext(),"onPreview Size :::" + potMap.size(),Toast.LENGTH_SHORT).show();
                progressDialog = ProgressDialog.show(this, "", resources.getString(R.string.preview));
                Thread previewThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            artVO = new ArtVO();
                            SaveImageUtil.saveArtInMemory(MainActivity.this, artLayout, artVO, originalBitmap);
                            new DatabaseManager(MainActivity.this).createArt(artVO);
                            handler.sendEmptyMessage(Constants.PHOTO_SAVED_SUCCESSFULLY);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                previewThread.start();
                break;
            case R.id.action_ungroup:
                setUngroupIteams();
                break;
            case R.id.action_delete_group:
                if (Global.isGroupCreated) {
                    horizontalListViewColor.setVisibility(View.GONE);
                    horizontalListViewPots.setVisibility(View.VISIBLE);
                    toggleButton.setImageResource(R.drawable.ic_action_image_color_lens);
                    toggleButton.setVisibility(View.VISIBLE);
                    isItemSelection = false;
                    for (CollageView collageView : iteamGroupSet) {
                        artLayout.removeView(collageView);
                        if (potMap.containsKey(collageView.getId())) {
                            potMap.remove(collageView.getId());
                        }
                    }
                    Global.targetView = null;
                    imgView = null;
                    imgViewForeground = null;
                    Global.isGroupCreated = false;
                    isLongPressed = false;
                    iteamGroupSet.clear();
                    menuUngroup.setVisible(false);
                    menuDeleteGroup.setVisible(false);
                    handleBottomUI(true);
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            switch (msg.what) {
                case Constants.PHOTO_SAVED_SUCCESSFULLY:
                    System.out.println("Total Size ::: " + potMap.size());
                    Intent intent = new Intent(MainActivity.this, PreviewActivity.class);
                    intent.putExtra("potmap", potMap);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("artVO", artVO);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    };


    public void slideShowBar(int animId, final View v) {

        Animation anim = AnimationUtils.loadAnimation(MainActivity.this,
                animId);

        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation paramAnimation) {
            }

            @Override
            public void onAnimationRepeat(Animation paramAnimation) {
            }

            @Override
            public void onAnimationEnd(Animation paramAnimation) {
                v.setVisibility(View.VISIBLE);
            }
        });
        v.startAnimation(anim);
    }

    public void slideHideBar(int animId, final View v) {

        Animation anim = AnimationUtils.loadAnimation(MainActivity.this,
                animId);

        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation paramAnimation) {
            }

            @Override
            public void onAnimationRepeat(Animation paramAnimation) {
            }

            @Override
            public void onAnimationEnd(Animation paramAnimation) {
                v.setVisibility(View.GONE);
            }
        });
        v.startAnimation(anim);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.lstColors && Global.targetView != null) {
            if (isColorAvailable) {
                ColorVO colorVO = colorVOs.get(position);
                imgView = (ImageView) Global.targetView.findViewById(R.id.imgView);
                imgViewForeground = (ImageView) Global.targetView.findViewById(R.id.imgViewTop);
                try {
                    if (grayScaledBitmap == null || !Ids.contains(Global.targetView.getId())) {
                        Ids.add(Global.targetView.getId());
                        progressDialog = ProgressDialog.show(MainActivity.this, "", "Please wait...");
                        BitmapDrawable bitmapDrawable = (BitmapDrawable) imgView.getDrawable();
                        grayScaledBitmap = Util.applyBlackWhite(bitmapDrawable.getBitmap());
                    /*bgTransparent = getTransparentBitmap(grayScaledBitmap, 255);*/
                        fgTransparent = Util.getTransparentBitmap(grayScaledBitmap, 120);
                        imgView.setImageBitmap(grayScaledBitmap);
                        imgViewForeground.setImageBitmap(fgTransparent);
                        progressDialog.dismiss();
                   /* if (horizontalListViewColor.getVisibility() == View.VISIBLE) {
                        slideHideBar(R.anim.slide_editor_bar_close_port, horizontalListViewColor);
                    }*/
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
                imgView.getDrawable().setColorFilter(Color.parseColor(colorVO.getColorCode()), PorterDuff.Mode.SRC_ATOP);
                ItemVO itemVO = potMap.get(Global.targetView.getId());
                itemVO.setColorName(colorVO.getColorName());
                itemVO.setColorCode(colorVO.getColorCode());
                potMap.put(Global.targetView.getId(), itemVO);
                colorVO = null;
            } else {
                Toast.makeText(MainActivity.this, resources.getString(R.string.no_color), Toast.LENGTH_SHORT).show();
            }
        } else if (parent.getId() == R.id.lstPots) {
            generateCollageView(potVOArrayList.get(position));
        }
    }

    private void generateCollageView(ItemVO potVO) {
        if (Global.isGroupCreated || isLongPressed) {
            setUngroupIteams();
        }
        if (btnClose != null && btnClose.getVisibility() == View.VISIBLE) {
            btnClose.setVisibility(View.GONE);
        }
        if (potVO.getColorId() == -1) {
            isColorAvailable = false;
        } else {
            isColorAvailable = true;
        }
        try {

            Drawable drawable = Drawable.createFromStream(getAssets().open(potVO.getDrawableName() + ".png"), null);

            Bitmap bm = ((BitmapDrawable) drawable).getBitmap();
            CollageView artView = new CollageView(MainActivity.this);
            artView.setId((int) new Date().getTime());
            RelativeLayout.LayoutParams artParam = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            artParam.addRule(RelativeLayout.CENTER_IN_PARENT);
            artView.setLayoutParams(artParam);

            ImageView imageViewBg = new ImageView(MainActivity.this);
            imageViewBg.setId(R.id.imgView);
            RelativeLayout.LayoutParams imgParamBg = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            imageViewBg.setLayoutParams(imgParamBg);
            imageViewBg.setPadding(20, 20, 20, 20);
            imageViewBg.setImageBitmap(bm);

            ImageView imageViewFg = new ImageView(MainActivity.this);
            imageViewFg.setId(R.id.imgViewTop);
            RelativeLayout.LayoutParams imgParamFg = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            imageViewFg.setLayoutParams(imgParamFg);
            imageViewFg.setPadding(20, 20, 20, 20);
            imageViewBg.setImageBitmap(bm);

            ImageButton imgButtonClose = new ImageButton(MainActivity.this);
            imgButtonClose.setId(R.id.btnClose);
            RelativeLayout.LayoutParams btnCloseParam = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            btnCloseParam.addRule(RelativeLayout.ALIGN_RIGHT, R.id.imgView);
            imgButtonClose.setLayoutParams(btnCloseParam);
            imgButtonClose.setVisibility(View.GONE);

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                imgButtonClose.setBackgroundResource(android.R.color.transparent);
            } else {
                imgButtonClose.setBackground(null);
            }

            imgButtonClose.setImageResource(R.drawable.ic_remove);

            artView.addView(imageViewBg, 0);
            artView.addView(imageViewFg, 1);
            artView.addView(imgButtonClose, 2);

            artView.setTag(potVO);

            potMap.put(artView.getId(), potVO);

            multiTouchListener = new MultiTouchListener(this, imgBackground, iteamGroupSet);

            multiTouchListener.setSingleTapListener(singleTapListener, potVO.getColorId() == -1 ? false : true);
            artView.setOnTouchListener(multiTouchListener);


            imgButtonClose.setOnClickListener(this);

            if (Global.targetView != null) {

                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    Global.targetView.setBackgroundResource(android.R.color.transparent);
                } else {
                    Global.targetView.setBackground(null);
                }
            }
            Global.targetView = artView;
            artLayout.addView(artView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (originalBitmap != null && !originalBitmap.isRecycled()) {
            originalBitmap.recycle();
            originalBitmap = null;
        }
    }

    private void initCategories() {
        currentSpinner = 1;
        firstCategorySpinner = (Spinner) findViewById(R.id.spinner_first_category);
        firstCategoryList = new ArrayList<>();
        firstCategoryList.add(new ItemVO(resources.getString(R.string.select_category), false, -10, -10, null, -10));
        CatalogueJsonParser.getAllSpecificItems(itemVOList, firstCategoryList, -1);

        firstCategoryAdapter = new ArrayAdapter<ItemVO>(this, android.R.layout.simple_spinner_dropdown_item, firstCategoryList);
        firstCategorySpinner.setAdapter(firstCategoryAdapter);
        firstCategorySpinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
     /*   potVOArrayList.clear();
        potsAdapter.notifyDataSetChanged();*/
        try {
            switch (parent.getId()) {
                case R.id.spinner_first_category:

                    if (secondCategorySpinner != null) {
                        secondCategorySpinner.setVisibility(View.INVISIBLE);
                    }
                    if (thirdCategorySpinner != null) {
                        thirdCategorySpinner.setVisibility(View.INVISIBLE);
                    }
                    if (fourthCatagorySpinner != null) {
                        fourthCatagorySpinner.setVisibility(View.INVISIBLE);
                    }

                    ItemVO selectedParentItem = firstCategoryList.get(position);
                    if (!selectedParentItem.getItemName().equals(resources.getString(R.string.select_category))) {

                        if (!selectedParentItem.isItem()) {
                            secondCategoryList = new ArrayList<>();
                            secondCategoryList.add(new ItemVO(resources.getString(R.string.select_subcategory), false, -10, -10, null, -10));
                            CatalogueJsonParser.getAllSpecificItems(itemVOList, secondCategoryList, selectedParentItem.getItemId());

                            secondCategorySpinner = (Spinner) findViewById(R.id.spinner_second_category);
                            secondCategoryAdapter = new ArrayAdapter<ItemVO>(this, android.R.layout.simple_list_item_1, secondCategoryList);
                            secondCategorySpinner.setAdapter(secondCategoryAdapter);

                            secondCategorySpinner.setOnItemSelectedListener(this);
                            secondCategorySpinner.setVisibility(View.VISIBLE);
                            currentSpinner = Constants.FIRST_SPINNER;
                        } else {
                            potVOArrayList.clear();
                            potsAdapter.notifyDataSetChanged();
                            potVOArrayList.add(selectedParentItem);
                            potsAdapter.notifyDataSetChanged();
                            generateCollageView(potVOArrayList.get(0));
                            horizontalListViewPots.setVisibility(View.GONE);
                            horizontalListViewPots.setVisibility(View.VISIBLE);
                            drawerLayout.closeDrawers();
                            if (layoutOptions.getVisibility() == View.GONE) {
                                handleBottomUI(true);
                            }
                        }
                    }
                    break;
                case R.id.spinner_second_category:

                    if (thirdCategorySpinner != null) {
                        thirdCategorySpinner.setVisibility(View.INVISIBLE);
                    }
                    if (fourthCatagorySpinner != null) {
                        fourthCatagorySpinner.setVisibility(View.INVISIBLE);
                    }
                    ItemVO selectedChildItem = secondCategoryList.get(position);
                    if (!selectedChildItem.getItemName().equals(resources.getString(R.string.select_subcategory))) {

                        if (!selectedChildItem.isItem()) {
                            thirdCategorySpinner = (Spinner) findViewById(R.id.spinner_third_category);
                            thirdCategoryList = new ArrayList<>();
                            CatalogueJsonParser.getAllSpecificItems(itemVOList, thirdCategoryList, selectedChildItem.getItemId());
                            currentSpinner = Constants.SECOND_SPINNER;
                            if (thirdCategoryList != null && thirdCategoryList.size() > 0) {
                                if (!thirdCategoryList.get(0).isItem()) {
                                    thirdCategoryAdapter = new ArrayAdapter<ItemVO>(this, android.R.layout.simple_list_item_1, thirdCategoryList);
                                    thirdCategoryList.add(0, new ItemVO(resources.getString(R.string.select_child_category), false, -10, -10, null, -10));
                                    thirdCategorySpinner.setAdapter(thirdCategoryAdapter);
                                    thirdCategorySpinner.setOnItemSelectedListener(this);
                                    thirdCategorySpinner.setVisibility(View.VISIBLE);

                                } else {
                                    potVOArrayList.clear();
                                    potsAdapter.notifyDataSetChanged();
                                    potVOArrayList.addAll(thirdCategoryList);
                                    potsAdapter.notifyDataSetChanged();
                                    horizontalListViewPots.setVisibility(View.GONE);
                                    horizontalListViewColor.setVisibility(View.GONE);
                                    horizontalListViewPots.setVisibility(View.VISIBLE);

                                    thirdCategoryAdapter = new ArrayAdapter<ItemVO>(this, android.R.layout.simple_list_item_1, thirdCategoryList);
                                    thirdCategoryList.add(0, new ItemVO(resources.getString(R.string.select_child_category), false, -10, -10, null, -10));
                                    thirdCategorySpinner.setAdapter(thirdCategoryAdapter);
                                    thirdCategorySpinner.setOnItemSelectedListener(this);
                                    thirdCategorySpinner.setVisibility(View.VISIBLE);

                                    if (layoutOptions.getVisibility() == View.GONE) {
                                        handleBottomUI(true);
                                    }
                                    // drawerLayout.closeDrawers();
                                }
                            }
                        } else {
                            potVOArrayList.clear();
                            potsAdapter.notifyDataSetChanged();
                            potVOArrayList.add(selectedChildItem);
                            potsAdapter.notifyDataSetChanged();
                            generateCollageView(potVOArrayList.get(0));
                            horizontalListViewColor.setVisibility(View.GONE);
                            horizontalListViewPots.setVisibility(View.GONE);
                            horizontalListViewPots.setVisibility(View.VISIBLE);
                            drawerLayout.closeDrawers();
                            if (layoutOptions.getVisibility() == View.GONE) {
                                handleBottomUI(true);
                            }
                        }
                    }
                    break;
                case R.id.spinner_third_category:

                    ItemVO selectedItem = thirdCategoryList.get(position);
                    if (!selectedItem.getItemName().equals(resources.getString(R.string.select_child_category))) {

                        //For Spinner
                        if (fourthCatagorySpinner != null) {
                            fourthCatagorySpinner.setVisibility(View.INVISIBLE);
                        } else {
                            fourthCatagorySpinner = (Spinner) findViewById(R.id.spinner_item_category);
                        }
                        fourthItemList = new ArrayList<>();
                        CatalogueJsonParser.getAllSpecificItems(itemVOList, fourthItemList, selectedItem.getItemId());
                        if (fourthItemList != null && fourthItemList.size() > 0) {
                            if (fourthItemList.get(0).isItem()) {
                                fourthCategoryAdapter = new ArrayAdapter<ItemVO>(this, android.R.layout.simple_list_item_1, fourthItemList);
                                fourthItemList.add(0, new ItemVO(resources.getString(R.string.select_last_category), false, -10, -10, null, -10));
                                fourthCatagorySpinner.setAdapter(fourthCategoryAdapter);
                                fourthCatagorySpinner.setOnItemSelectedListener(this);
                                fourthCatagorySpinner.setVisibility(View.VISIBLE);
                                currentSpinner = Constants.THIRD_SPINNER;
                            }
                        }
                        //For Horizontal LisView
                        if (!selectedItem.isItem()) {
                            potVOArrayList.clear();
                            potsAdapter.notifyDataSetChanged();
                            CatalogueJsonParser.getAllSpecificItems(itemVOList, potVOArrayList, selectedItem.getItemId());
                        } else {
                            // potVOArrayList.add(selectedItem);
                            generateCollageView(potVOArrayList.get(position - 1));
                            drawerLayout.closeDrawers();
                        }
                        Log.d("POT VO", "POT VO Size: " + potVOArrayList.size());
                        potsAdapter.notifyDataSetChanged();
                        // drawerLayout.closeDrawers();
                        horizontalListViewColor.setVisibility(View.GONE);
                        horizontalListViewPots.setVisibility(View.GONE);
                        horizontalListViewPots.setVisibility(View.VISIBLE);

                        if (layoutOptions.getVisibility() == View.GONE) {
                            handleBottomUI(true);
                        }
                    }
                    break;
                case R.id.spinner_item_category:
                    if (position > 0 && fourthItemList != null && fourthItemList.size() > 0) {
                        generateCollageView(fourthItemList.get(position));
                        drawerLayout.closeDrawers();
                    }
                    if (layoutOptions.getVisibility() == View.GONE) {
                        handleBottomUI(true);
                    }
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onMediaScannerConnected() {
        try {
            mediaScannerConnection.scanFile(filePath, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {

    }

   /* public void getTempJson() {
        try {
            int i = 1;
            JSONObject jsonObj = new JSONObject();
            JSONArray colorArray = new JSONArray();
            for (ColorVO color : colorVOs) {

                String colorName = color.getColorName();
                String colorCode = color.getColorCode();

                JSONObject colorJsonObj = new JSONObject();
                colorJsonObj.put("color_id", String.valueOf(i));
                colorJsonObj.put("color_name", colorName);
                colorJsonObj.put("color_code", colorCode);
                i++;
                colorArray.put(colorJsonObj);
            }
            jsonObj.put("colors", colorArray);
            File file = new File(Environment.getExternalStorageDirectory() + "/json.txt");
            PrintStream out = new PrintStream(file);
            out.print(jsonObj.toString());
            Log.d(TAG, jsonObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static MultiTouchListener getMultiTouchListener() {
        return multiTouchListener;
    }

    private void setUngroupIteams() {
        if (iteamGroupSet.size() > 0 && Global.isGroupCreated) {
            for (CollageView collageView : iteamGroupSet) {
                    collageView.setOnTouchListener(multiTouchListener);
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        collageView.setBackgroundResource(android.R.color.transparent);
                    } else {
                        collageView.setBackground(null);
                    }
            }
        }
        isLongPressed = false;
        Global.isGroupCreated = false;
        isLongPressed = false;
        iteamGroupSet.clear();
        menuUngroup.setVisible(false);
        menuDeleteGroup.setVisible(false);
        handleBottomUI(true);
        horizontalListViewPots.setVisibility(View.VISIBLE);
    }
}


