package com.spec.ambius.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kunalk on 12/18/2014.
 */
public class ColorVO implements Parcelable {
    private int colorId;
    private String colorName;

    private String colorCode;

    public ColorVO() {
    }

    public ColorVO(int colorId, String colorName, String colorCode) {
        this.colorName = colorName;
        this.colorCode = colorCode;
        this.colorId = colorId;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) // (3)
    {
        out.writeString(getColorName());
        out.writeString(getColorCode());
    }

    private static ColorVO readFromParcel(Parcel in) { // (4)
        ColorVO colorVO = new ColorVO();
        colorVO.setColorName(in.readString());
        colorVO.setColorCode(in.readString());
        return colorVO;
    }

    public static final Creator CREATOR = new Creator() // (5)
    {
        public ColorVO createFromParcel(Parcel in) // (6)
        {
            return readFromParcel(in);
        }

        @Override
        public Object[] newArray(int size) {
            return new Object[0];
        }
    };
}
