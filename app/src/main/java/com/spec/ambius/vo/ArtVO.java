package com.spec.ambius.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kunalk on 12/3/2014.
 */
public class ArtVO implements Parcelable{
    private int id;
    private String editedImagePath;
    private String originalImagePath;
    private String thumbImagePath;

    public ArtVO(){}

    public String getEditedImagePath() {
        return editedImagePath;
    }

    public void setEditedImagePath(String editedImagePath) {
        this.editedImagePath = editedImagePath;
    }

    public String getOriginalImagePath() {
        return originalImagePath;
    }

    public void setOriginalImagePath(String originalImagePath) {
        this.originalImagePath = originalImagePath;
    }

    public String getThumbImagePath() {
        return thumbImagePath;
    }

    public void setThumbImagePath(String thumbImagePath) {
        this.thumbImagePath = thumbImagePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) // (3)
    {
        out.writeInt(getId());
        out.writeString(getEditedImagePath());
        out.writeString(getOriginalImagePath());
        out.writeString(getThumbImagePath());
    }

    private static ArtVO readFromParcel(Parcel in) { // (4)
        ArtVO artVO = new ArtVO();
        artVO.setId(in.readInt());
        artVO.setEditedImagePath(in.readString());
        artVO.setOriginalImagePath(in.readString());
        artVO.setThumbImagePath(in.readString());
        return artVO;
    }

    public static final Creator CREATOR = new Creator() // (5)
    {
        public ArtVO createFromParcel(Parcel in) // (6)
        {
            return readFromParcel(in);
        }

        @Override
        public Object[] newArray(int size) {
            return new Object[0];
        }
    };
}
