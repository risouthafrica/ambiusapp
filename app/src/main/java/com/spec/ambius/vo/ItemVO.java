package com.spec.ambius.vo;

import java.io.Serializable;

/**
 * Created by umangk on 29-Dec-14.
 */
public class ItemVO implements Serializable {

    private int itemId, parentId;
    private String itemName, drawableName, colorName, colorCode;

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    private int colorId;

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    private boolean isItem;



    public ItemVO(String itemName, boolean isItem, int itemId, int parentId, String drawableName, int colorId) {
        this.itemName = itemName;
        this.isItem = isItem;
        this.itemId = itemId;
        this.parentId = parentId;
        this.drawableName = drawableName;
        this.colorId = colorId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isItem() {
        return isItem;
    }

    public void setItem(boolean isItem) {
        this.isItem = isItem;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return this.getItemName();
    }

    public String getDrawableName() {
        return drawableName;
    }

    public void setDrawableName(String drawableName) {
        this.drawableName = drawableName;
    }

}
