package com.spec.ambius.vo;

import java.io.Serializable;

/**
 * Created by umangk on 15-Dec-14.
 */
public class PotVO implements Serializable {

    private int potDrawable;
    private String potName;
    private String colorName, colorCode;

    public PotVO() {
    }

    public PotVO(int potDrawable, String potName) {
        this.potDrawable = potDrawable;
        this.potName = potName;
    }

    public int getPotDrawable() {
        return potDrawable;
    }

    public void setPotDrawable(int potDrawable) {

        this.potDrawable = potDrawable;
    }

    public String getPotName() {

        return potName;
    }

    public void setPotName(String potName) {

        this.potName = potName;
    }

    /* @Override
     public int describeContents() {
         return 0;
     }

     @Override
     public void writeToParcel(Parcel out, int flags) // (3)
     {
         out.writeString(getPotName());
         out.writeString(getColorName());
         out.writeString(getColorCode());
     }

     private static PotVO readFromParcel(Parcel in) { // (4)
         PotVO potVO = new PotVO();
         potVO.setPotName(in.readString());
         potVO.setColorName(in.readString());
         potVO.setColorCode(in.readString());
         return potVO;
     }

     public static final Parcelable.Creator CREATOR = new Parcelable.Creator() // (5)
     {
         public PotVO createFromParcel(Parcel in) // (6)
         {
             return readFromParcel(in);
         }

         @Override
         public Object[] newArray(int size) {
             return new Object[0];
         }
     };
 */
    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }
}
