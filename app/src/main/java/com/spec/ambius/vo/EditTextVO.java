package com.spec.ambius.vo;

import android.widget.EditText;

/**
 * Created by umangk on 13-Jan-15.
 */
public class EditTextVO {

    private EditText editText;
    private boolean isLast;

    public EditTextVO(EditText editText, boolean isLast) {
        this.editText = editText;
        this.isLast = isLast;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean isLast) {
        this.isLast = isLast;
    }
}
