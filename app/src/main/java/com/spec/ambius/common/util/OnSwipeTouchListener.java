package com.spec.ambius.common.util;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class OnSwipeTouchListener implements OnTouchListener {

	private String TAG = "OnSwipeTouchListener";
	@SuppressWarnings("deprecation")
	public GestureDetector gestureDetector = new GestureDetector(
			new GestureListener());
	private int height;
	private int width;
	private int orientation;

	public boolean onTouch(final View view, final MotionEvent motionEvent) {
		return gestureDetector.onTouchEvent(motionEvent);
	}

	public OnSwipeTouchListener(Activity context) {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		/*context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);*/
		height = context.getResources().getDisplayMetrics().heightPixels;
		width = context.getResources().getDisplayMetrics().widthPixels;
		orientation = context.getResources().getConfiguration().orientation;
	}

	private final class GestureListener extends SimpleOnGestureListener {

		private static final int SWIPE_THRESHOLD = 50;
		private static final int SWIPE_VELOCITY_THRESHOLD = 50;

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            onSingleTap();
            return true;
        }

        @Override
		public boolean onDown(MotionEvent e) {
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			boolean result = false;
			try {
				Log.d(TAG, "Height : " + height + "Width : " + width);
				float diffY = e2.getY() - e1.getY();
				float diffX = e2.getX() - e1.getX();

                Log.d(TAG, "Top & Bottom" + e1.getY() + "e2 :" + e2.getY());
                if (Math.abs(diffY) > SWIPE_THRESHOLD
                        && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    Log.d(TAG, "Y");
                    if (e1.getY() > (height - 400)) {
                        Log.d(TAG, "Bottom");
                        if (e2.getY() - e1.getY() < 0) {
                            Log.d(TAG, "OpenBottom");
                            onSwipeBottom();

                        } else {
                            Log.d(TAG, "CloseBottom");
                            onSwipeCloseBottom();
                        }
                    }
                }
			} catch (Exception exception) {
				exception.printStackTrace();
			}
			return result;
		}
	}

    public void onSingleTap(){}

	public void onSwipeRight() {
	}

	public void onSwipeLeft() {
	}

	public void onSwipeTop() {
	}

	public void onSwipeBottom() {
	}

	public void onSwipeCloseLeft() {
	}

	public void onSwipeCloseRight() {
	}

	public void onSwipeCloseBottom() {
	}

	public void onSwipeCloseTop() {
	}

	public int getOrientation() {
		// TODO Auto-generated method stub
		return orientation;
	}

	public void setOrientation(int orienation) {
		// TODO Auto-generated method stub
		this.orientation = orienation;
	}
}