package com.spec.ambius.common.callbackinterface;

import com.google.android.gms.auth.UserRecoverableAuthException;

/**
 * Created by namrata on 2/7/2015.
 * @interface IOnUserRecoverableException is used as a callback for handling UserRecoverableException. This exception
 * is thrown for the first time when the user installs the app. It explicitly asks the user to grant the permissions.
 * Once the permission is given, user need not give it everytime.
 */
public interface IOnUserRecoverableException {
     void onUserRecoverableException(UserRecoverableAuthException exception);
//     void enableAddRecordButton(List<SpreadsheetEntry> spreadsheetEntryList);
}
