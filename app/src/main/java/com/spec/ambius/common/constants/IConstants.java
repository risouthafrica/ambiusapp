package com.spec.ambius.common.constants;

/**
 * Created by kunalk on 12/3/2014.
 */
public interface IConstants {
    String TABLE_ART="tblArt";
    String COLUMN_ID="image_id";
    String COLUMN_IMG_PATH_EDITED="edited_img_path";
    String COLUMN_IMG_PATH_ORIGINAL="original_img_path";
    String COLUMN_IMG_PATH_THUMB="thumb_img_path";

    String TABLE_COLOR="tblColor";
    String COLOR_ID="color_id";
    String COLOR_NAME="color_name";
    String COLOR_CODE="color_code";

    String JUST_ONCE = "Just Once";
    String ALWAYS = "Always";

    String CHECK_PREFERENCE = "CHECK_PREFERENCE";
    String OPEN_PREFFERED_EMAIL_CLIENT = "OPEN_PREFFERED_EMAIL_CLIENT";
    String PREFFERED_MAIL_CLIENT_COMPOSE_ACTIVITY = "PREFFERED_MAIL_CLIENT_COMPOSE_ACTIVITY";

    String DEFAULT_PACKAGE_NAME = "com.google.android.gm";
    String DEFAULT_COMPOSE_ACTIVITY = "com.google.android.gm.ComposeActivityGmail";
    String DEFAULT_SUBJECT = "Test";

}
