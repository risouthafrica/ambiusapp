package com.spec.ambius.common.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.spec.ambius.common.constants.Constants;
import com.spec.ambius.vo.ArtVO;

import java.util.ArrayList;

/**
 * Created by kunalk on 12/3/2014.
 */
public class DatabaseManager {
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    public DatabaseManager(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

  /*  public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }
*/
  /*  public void close() {
        dbHelper.close();
    }*/

    public long createArt(ArtVO artVO) {
        database = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.COLUMN_IMG_PATH_EDITED, artVO.getEditedImagePath());
        contentValues.put(Constants.COLUMN_IMG_PATH_ORIGINAL, artVO.getOriginalImagePath());
        contentValues.put(Constants.COLUMN_IMG_PATH_THUMB, artVO.getThumbImagePath());

        long l = database.insert(Constants.TABLE_ART, null, contentValues);
        database.close();
        return l;
    }

    public ArrayList<ArtVO> getAllArts() {

        ArrayList<ArtVO> artVOs = new ArrayList<ArtVO>();
        database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_ART, new String[]{"*"}, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                ArtVO artVO = new ArtVO();
                artVO.setId(cursor.getInt(0));
                artVO.setEditedImagePath(cursor.getString(1));
                artVO.setOriginalImagePath(cursor.getString(2));
                artVO.setThumbImagePath(cursor.getString(3));
                artVOs.add(artVO);
            } while (cursor.moveToNext());
        }
        database.close();
        return artVOs;
    }

    public void deleteArt(int id) {
        database = dbHelper.getWritableDatabase();
        database.delete(Constants.TABLE_ART, Constants.COLUMN_ID + "=" + id, null);
        database.close();
    }

  /*  public boolean isColorAvailable() {
        database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Constants.TABLE_COLOR, new String[]{"*"}, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            return true;
        }
        database.close();
        return false;
    }

    public void insertColors() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("WHITE PL", "#DAE5DC");
        hashMap.put("CREAM WHITE", "#EEE1C5");
        hashMap.put("WHITE", "#FFFFFF");
        hashMap.put("NUTMEG", "#E0C398");
        hashMap.put("LEMON CHROME", "#FFDD00");
        hashMap.put("MID CHROME", "#FFC424");
        hashMap.put("SCARLETTE CHROME", "#F15D22");
        hashMap.put("MELON YELLOW", "#FCB033");
        hashMap.put("DAISY YELLOW", "#FFEEBB");
        hashMap.put("CORAL", "#FFE1BA");
        hashMap.put("WHEAT FIELD", "#FFE19B");
        hashMap.put("OREGON", "#DDAF43");
        hashMap.put("MEXICAN TAN", "#D59F0F");
        hashMap.put("BUTTERCUP", "#FFDE6C");
        hashMap.put("GOLD BUST", "#FFD457");
        hashMap.put("OLD GOLD", "#FFE14F");
        hashMap.put("MOCK ORANGE", "#FDB813");
        hashMap.put("DEEP ORANGE", "#F47421");
        hashMap.put("LIGHT ORANGE", "#F38F1D");
        hashMap.put("TERRACOTTA", "#BF311A");
        hashMap.put("BURNT VANILLA", "#FBAF5F");
        hashMap.put("COFFEE CREAME", "#FDC180");
        hashMap.put("VANILLA BUD", "#FAA633");
        hashMap.put("SURE APPRICOT", "#F99D31");
        hashMap.put("SWEET SUN", "#FED29D");
        hashMap.put("PINK VANILLA", "#FDC180");
        hashMap.put("TINTED MUSHROOM", "#FBB480");
        hashMap.put("LIGHT SIENNA", "#CE7019");
        hashMap.put("APRICOT BURST", "#B06010");
        hashMap.put("RUBY RED", "#B30738");
        hashMap.put("SINGAL RED", "#E31B23");
        hashMap.put("POST OFFICE RED", "#C41130");
        hashMap.put("DARK BURGUNDY", "#762123");
        hashMap.put("LIGHT BURGUNDY", "#B45240");
        hashMap.put("POST OFFICE RED ST", "#BD1F35");
        hashMap.put("MEMONES", "#F79C88");
        hashMap.put("DUSKY ROSE", "#EE3424");
        hashMap.put("PINOTAGE", "#98002E");
        hashMap.put("VOODOO PINL", "#F5E0ED");
        hashMap.put("YOU ARE A PEACH", "#EBDDDD");
        hashMap.put("CRIMSON", "#EAE2DC");
        hashMap.put("SHEER DELITE", "#DCD1CD");
        hashMap.put("VANILLA CREME", "#F4DED6");
        hashMap.put("PINK SUEDE", "#FDE5DF");
        hashMap.put("HAZY HUE", "#E1B6B2");
        hashMap.put("SMOKY HASE", "#EAB4B8");
        hashMap.put("TAUPE TONED", "#E1A3A0");
        hashMap.put("SUNSET IN BALI", "#F48F9D");
        hashMap.put("DUSK GLOW", "#F15F7C");
        hashMap.put("ROSE ROSE", "#E1134F");
        hashMap.put("ROSE DUST", "#AB0534");
        hashMap.put("HOT MELON", "#A40046");
        hashMap.put("TELEMAGENTA", "#C6006F");
        hashMap.put("YOUNGBERRY WINE", "#EEEEF7");

        hashMap.put("MULBERRY MUNCH", "#DFD7EB");
        hashMap.put("SOFT SERENADE", "#B5B2D9");
        hashMap.put("LILAC LUSTRE", "#A3A0C7");
        hashMap.put("EXOTICE MAUVE", "#E6CEE4");
        hashMap.put("VIOLET HUE", "#D4AED2");
        hashMap.put("DEEP IRIS", "#B195C6");
        hashMap.put("MULBERRY", "#9B5BA4");
        hashMap.put("PURPLE PEOPLE", "#6E298D");
        hashMap.put("DARK VIOLET", "#650260");
        hashMap.put("PERAL NIGHT BLUE", "#11155E");
        hashMap.put("AZURE BLUE", "#004A8D");
        hashMap.put("PERSIAN BLUE", "#002C55");
        hashMap.put("CORNFLOWER BLUE", "#6799C8");
        hashMap.put("TURQUOISE", "#35BDB2");
        hashMap.put("TURQUOISE BLUE", "#70CDE3");
        hashMap.put("BLUEBERRY PIE", "#00A4E4");
        hashMap.put("PALACE BLUE", "#009AC8");
        hashMap.put("MIST OF MAY", "#0083A9");
        hashMap.put("DUSKEY GREY", "#00718F");
        hashMap.put("SEA SHELL", "#CEEBEA");
        hashMap.put("WATER DANCE", "#9AD7DB");
        hashMap.put("GREEN JADE", "#67C8C6");
        hashMap.put("GREEN BERET", "#00A0AF");
        hashMap.put("GREEN SWAMP", "#007E7A");
        hashMap.put("HEAVENLY BLUE", "#CFE0F3");
        hashMap.put("PRUSSIAN BLUE", "#729AC7");
        hashMap.put("STANDARD DARK GREEN", "#004711");
        hashMap.put("APPLE GREEN", "#48A942");
        hashMap.put("AVO GREEN", "#9FA617");
        hashMap.put("LIME GREEN PL", "#8CC63F");
        hashMap.put("LINDEN GREEN", "#8DBD3F");
        hashMap.put("LIME GREEN ATD", "#B2BB1D");
        hashMap.put("GREEN CORN", "#BAE0CD");
        hashMap.put("SUMMER SAVORY", "#73C6A1");
        hashMap.put("CURLEY LEAF", "#9FD5BC");
        hashMap.put("PALE APPLE", "#48A942");
        hashMap.put("GREEN PLUM", "#008265");
        hashMap.put("GREEN GRASS", "#B3D88C");
        hashMap.put("GREENLEEVES", "#D0E4A6");
        hashMap.put("GREEN FIR", "#7BC142");
        hashMap.put("PEPPERMINT GREEN", "#6DB33F");
        hashMap.put("GREEN FERN", "#439539");
        hashMap.put("SAP GREEN", "#EEF6E7");
        hashMap.put("SPRING LEAF", "#DBE8C4");
        hashMap.put("SOFT GREEN", "#C8D5A6");
        hashMap.put("OLD MOSS", "#A9C399");
        hashMap.put("BLACK PL", "#241A04");
        hashMap.put("CHOCOLATE BROWN", "#572700");
        hashMap.put("NIGHT BLACK", "#000000");
        hashMap.put("PEBBLE GREY", "#CBC1B6");
        hashMap.put("LIGHT GREY", "#939BA1");
        hashMap.put("DARK SEA GREY", "#455560");
        hashMap.put("CHARCOAL", "#5F6062");
        hashMap.put("PEBBLE GREY ST", "#A6A392");
        hashMap.put("QUICK SAND", "#BDBCAF");
        hashMap.put("METALLIC", "#A6A698");
        hashMap.put("APACHE DAWN", "#949384");
        hashMap.put("SIERRA RANGE", "#757561");
        hashMap.put("ECLIPSE", "#02180D");
        hashMap.put("WATERBESSIE", "#FFF0D9");
        hashMap.put("FRESH CREAM", "#FFF6DC");
        hashMap.put("YELLOW SILK", "#E7E5D3");
        hashMap.put("CHUTNEY", "#D8D0C7");
        hashMap.put("KARAKUL", "#7E5439");
        hashMap.put("SPICED CARAMEL", "#5A471B");
        hashMap.put("NATURAL PEACH", "#914D02");
        hashMap.put("STAR BRONZER", "#7C351F");
        hashMap.put("RUSTIC", "#5A1300");
        hashMap.put("CHERRY RED", "#8A1E03");
        hashMap.put("WILD GINGER", "#FFFAC4");
        hashMap.put("SUTHERLANDIA", "#F6F1CD");
        hashMap.put("GREEN OASIS", "#F1E5C8");
        hashMap.put("CAMPHOR", "#ECDFA8");
        hashMap.put("GINGER BUSH", "#CBB677");
        hashMap.put("BULRUSH", "#E9D666");
        hashMap.put("MARAMA BEAN", "#F4DC00");
        hashMap.put("AGATE", "#D7C300");
        hashMap.put("GEMSBOK", "#C0AD00");
        hashMap.put("ACASIA", "#A28700");
        hashMap.put("ESPRESSO", "#231100");

        database = dbHelper.getWritableDatabase();
        for (String key : hashMap.keySet()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Constants.COLOR_NAME, key);
            contentValues.put(Constants.COLOR_CODE, hashMap.get(key));
            database.insert(Constants.TABLE_COLOR, null, contentValues);
            contentValues = null;
        }
        database.close();
    }*/

    /*public ArrayList<ColorVO> getColorList() {
        database = dbHelper.getWritableDatabase();
        ArrayList<ColorVO> colorVOs = new ArrayList<ColorVO>();
        Cursor cursor = database.query(Constants.TABLE_COLOR, new String[]{"*"}, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                ColorVO colorVO = new ColorVO(cursor.getString(1), cursor.getString(2));
                colorVOs.add(colorVO);
            } while (cursor.moveToNext());
        }
        database.close();
        return colorVOs;
    }*/
}
