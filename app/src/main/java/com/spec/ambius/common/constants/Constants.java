package com.spec.ambius.common.constants;

import java.util.regex.Pattern;

/**
 * Created by umangk on 23-Dec-14.
 */
public class Constants {
    public static final int PHOTO_SAVED_SUCCESSFULLY = 0;
    public static final int IMAGE_PICK = 1;
    public static final int CAMERA_REQUEST = 2;

    public static final int DEFAULT_SPINNER = 0;
    public static final int FIRST_SPINNER = 1;
    public static final int SECOND_SPINNER = 2;
    public static final int THIRD_SPINNER = 3;

    //database related
    public static final String TABLE_ART = "tblArt";
    public static final String COLUMN_ID = "image_id";
    public static final String COLUMN_IMG_PATH_EDITED = "edited_img_path";
    public static final String COLUMN_IMG_PATH_ORIGINAL = "original_img_path";
    public static final String COLUMN_IMG_PATH_THUMB = "thumb_img_path";

   /* public static final String TABLE_COLOR = "tblColor";
    public static final String COLOR_ID = "color_id";
    public static final String COLOR_NAME = "color_name";
    public static final String COLOR_CODE = "color_code";*/

    //Date Format
     public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    //email related
    public static final String JUST_ONCE = "Just Once";
    public static final String ALWAYS = "Always";

    public static final String CHECK_PREFERENCE = "CHECK_PREFERENCE";
    public static final String OPEN_PREFFERED_EMAIL_CLIENT = "OPEN_PREFFERED_EMAIL_CLIENT";
    public static final String PREFFERED_MAIL_CLIENT_COMPOSE_ACTIVITY = "PREFFERED_MAIL_CLIENT_COMPOSE_ACTIVITY";

    public static final String DEFAULT_PACKAGE_NAME = "com.google.android.gm";
    public static final String DEFAULT_COMPOSE_ACTIVITY = "com.google.android.gm.ComposeActivityGmail";
    public static final String DEFAULT_SUBJECT = "Ambius VIP";

    public static final int TYPE_WIFI = 1;
    public static final int TYPE_MOBILE = 2;
    public static final int TYPE_NOT_CONNECTED = 0;
    public static final String TYPE_WIFI_STRING = "TYPE_WIFI";
    public static final String TYPE_MOBILE_STRING = "TYPE_MOBILE";
    public static final String TYPE_NOT_CONNECTED_STRING = "TYPE_NOT_CONNECTED";
   // public static final String CONNECTION_ERROR = "Please check your internet connection";
    public static final String ERROR_WHILE_CONFIGURING = "An error occured while configuring.. Please try again!!";
    public static final String PROGRESS_DIALOG_MESSAGE = "Signing in...";
    public static final String PROGRESS_DIALOG_LOADING = "Loading...";
    public static final String CONNECTION_SUSPENDED = "Connection Suspended..Try Reconnecting..";
    public static final String CONNECTION_FAILED = "Failed to connect! Do you want to retry?";
    public static final String INVALID_DOMAIN_ERROR = "Invalid Domain Error!" +"\n" + "Please enter Email Address with domain gmail.com";
    public static final String INVALID_EMAIL_ID = "Please enter a valid Email Id";
    public static final String EMPTY_EMAIL_ID_ERROR = "Please select Email Id";
    public static final String ADD_EMAIL_ID = "Please add Email Id";
    public static final String SELECT_EMAIL_ID = "Select Email Id";
    //Client Id that we get when we register our project to Google developer console
    public static final String CLIENT_ID = "503351511688-cpkke4jgq1mv0e4fi7uj07fro91061av.apps.googleusercontent.com";
	/*API_Key that we get when we register our project to Google developer console, this can be used for getting activity feeds
    from this url in json form : https://www.googleapis.com/plus/v1/people/mUserIdOfCurrentUser/activities/public?key=YOUR_API_KEY  */
    public static final String API_KEY = "AIzaSyBk9GtfQpPgrx1ig2u8e7JCWYHsCJLJM_A";
    public static final String DISPLAY_NAME = "DISPLAY_NAME";
    public static final String USER_ID = "USER_ID";
    public static final String OK = "OK";
    public static final String CANCEL = "CANCEL";
    //Regular Expression Pattern that we use to validate email
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );
    //Account type that we require
    public static final String ACCOUNT_TYPE = "com.google";
    //Email Id domain that we aim to authenticate, here we use "gmail.com"
    public static final String EMAIL_ID_DOMAIN = "gmail.com";
    public static final String  SIGN_IN_SUCCESSFUL_MESSAGE = "Sign In Successful";

    public static final String BLANK_STRING = "";

    //Google SpreadSheet
    public static final String REQUIRED_SPREADSHEET_TITLE = "Ambius VIP data";
    public static final String REQUIRED_LOGIN_WORKSHEET_TITLE = "logindetail";
    public static final String REQUIRED_ITEM_WORKSHEET_TITLE = "itemdetail";
    public static final String SPREAD_SHEET_LIST_NULL = "Spreadsheet List is null or empty";
    public static final String WORK_SHEET_LIST_NULL = "WorkSheet List is null or empty";
    public static final String SPREAD_SHEET_ENTRY_NULL = "Spreadsheet entry is null.Cannot find the requested file";
    public static final String WORK_SHEET_ENTRY_NULL = "WorkSheet entry is null.Cannot find the requested file";
    public static final String INTENT_DATA_NULL = "Intent data is null";
    public static final String ACCOUNT_NAME_NULL_OR_EMPTY = "Account name is null or empty";
    public static final String ACCESS_TOKEN_NULL = "Access token is null or empty";
    public static final int TIMEOUT_CONNECTION = 5000;//5sec
    public static final int TIMEOUT_SOCKET = 30000;//30sec
    public static final String TAG_ERROR = "error";
    public static final String TAG_EXPIRES_IN = "expires_in";
    public static final String INVALID_TOKEN = "invalid_token";
    public static final String ROW_INSERTED_SUCCESSFULLY = "Row inserted successfully";
    public static final String CHOOSER_CANCELLED = "You cancelled chooser";
    public static final String COLUMN_DATE_TIME =  "DateTime";
    public static final String COLUMN_USER_EMAIL  = "UserEmail";
    public static final String COLUMN_TARGET_EMAIL  = "TargetEmail";
    public static final String COLUMN_CATEGORY  = "Category";
    public static final String COLUMN_ITEM_NAME  = "ItemName";
    public static final String SPRED_SHEET_FILE_NAME = "Rentokil Ambius";
    public static final String MyPREFERENCES = "userEmailPref" ;
    public static final String USER_EMAIL_PREF = "EmailAddress";
    public static final String IS_NEED_TO_INSERT = "isNeedToInsert";
    public static final int INSERT_ITEM_DETAILS = 1;
    public static final int NO_SPREAD_SHEET_AVAILABLE = 2;
    public static final int NO_SPREAD_TAB_AVAILABLE = 3;
    public static final int NO_EDIT_ACCESS = 4;
    public static final int NO_USER_EMAIL = 5;
    public static final int NO_DATE_TIME = 6;
    public static final int NO_CATEGORY = 7;
    public static final int NO_ITEM_NAME = 8;
    public static final int NO_TARGET_EMAIL = 9;
    public static final int INTERNET_CONNECTION = 10;
    public static final int NO_AUTHORIZED = 11;
    public static final String CHECK_COLOUMN_USER_EMAIL = "useremail";
    public static final String CHECK_COLOUMN_DATE_TIME = "datetime";
    public static final String CHECK_COLOUMN_CATEGORY = "category";
    public static final String CHECK_COLOUMN_ITEM_NAME = "itemname";
    public static final String CHECK_COLOUMN_TARGET_EMAIL = "targetemail";
    // One can change private to public and full to basic depending upon the requirements
    public static final String SPREADSHEET_FEED_URL  = "https://spreadsheets.google.com/feeds/spreadsheets/private/full";
}
