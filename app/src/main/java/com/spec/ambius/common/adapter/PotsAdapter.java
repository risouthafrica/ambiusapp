package com.spec.ambius.common.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spec.ambius.R;
import com.spec.ambius.vo.ItemVO;

import java.util.ArrayList;

/**
 * An array adapter that knows how to render views when given CustomData classes
 */
public class PotsAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    //private int[] mPots;
    private Context mContext;
    private ArrayList<ItemVO> potVOArrayList;
    private Resources resources;

    public PotsAdapter(Context context, ArrayList<ItemVO> potVOArrayList) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //this.mPots = pots;
        this.potVOArrayList = potVOArrayList;
        this.mContext = context;
        resources = mContext.getResources();
    }

    @Override
    public int getCount() {

        //return mPots.length;
        return potVOArrayList.size();
    }

    @Override
    public Object getItem(int position) {

        //return mPots[position];
        return potVOArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.layout_pot, parent, false);
            holder = new Holder();
            holder.imgPot = (ImageView) convertView.findViewById(R.id.imgPot);
            holder.potName = (TextView) convertView.findViewById(R.id.pot_name);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        //holder.imgPot.setImageResource(mPots[position]);

/*        int resourceId = resources.getIdentifier(potVOArrayList.get(position).getDrawableName(), "drawable",
                mContext.getPackageName());
        holder.imgPot.setImageResource(resourceId);*/
        try {
            ItemVO item = potVOArrayList.get(position);
            Bitmap bm = ((BitmapDrawable) Drawable.createFromStream(mContext.getAssets().open(item.getDrawableName() + ".png"), null)).getBitmap();
            //holder.imgPot.setImageDrawable(Drawable.createFromStream(mContext.getAssets().open(potVOArrayList.get(position).getDrawableName() + ".png"), null));
            //holder.imgPot.setImageBitmap(Util.adjustBitmap(bm));
            holder.imgPot.setImageBitmap(bm);
            if (item.getParentId() >= 58 && item.getParentId() <= 71) {
                holder.potName.setText(item.getItemName().substring(item.getItemName().length() - 1));
            } else {
                holder.potName.setText(item.getItemName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    /**
     * View holder for the views we need access to
     */
    private static class Holder {
        public ImageView imgPot;
        public TextView potName;
    }
}
