package com.spec.ambius.common.util;

public interface OnDialogActionListener {
	void onClick(int id, int secondId);
}
