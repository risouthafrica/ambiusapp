package com.spec.ambius.common.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.RelativeLayout;

import com.spec.ambius.vo.ArtVO;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by umangk on 23-Dec-14.
 */

public class SaveImageUtil {


    public static void saveArtInMemory(Context context, RelativeLayout artLayout, ArtVO artVO, Bitmap originalBitmap) throws Exception {

        Date date = new Date(System.currentTimeMillis());

        //String time = String.valueOf();
        String time = getTime(date);
        String imgMeasurement = "img_rentokilambius_" + time + ".jpg";
        final int[] location = new int[2];
        artLayout.getLocationOnScreen(location);
        artLayout.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(calculateHeightWidth(artLayout.getDrawingCache()));//, location[0], location[1], location[0]+artLayout.getWidth(), location[1]+artLayout.getHeight());
        artLayout.setDrawingCacheEnabled(false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes);

        File file = null;
        if (isExternalStrorageAvailable()) {
            File folder = new File(Environment.getExternalStorageDirectory() + "/Ambius-VIP");
            if (!folder.exists()) {
                folder.mkdir();
            }
            String fileName = folder.getAbsolutePath()
                    + File.separator + imgMeasurement;
            file = new File(fileName);
        } else {
            String fileName = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                    + File.separator + imgMeasurement;
            file = new File(fileName);
        }

        try {
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            fo.close();
            bytes.close();
            artVO.setEditedImagePath(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bitmap = null;
            imgMeasurement = null;
        }

        saveOriginalBitmap(time, context, artVO, originalBitmap);

        saveThumbnail(file.getAbsolutePath(), time, context, artVO);
    }

    private static void saveOriginalBitmap(String time, Context context, ArtVO artVO, Bitmap originalBitmap) throws IOException {
        String imgOriginal = "Original_" + time + ".jpg";
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        originalBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        File f = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                + File.separator + imgOriginal);
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
            bytes.close();
            artVO.setOriginalImagePath(f.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            originalBitmap = null;
        }
    }

    private static void saveThumbnail(String path, String time, Context context, ArtVO artVO) throws Exception {
        String imgThumbnail = "Thumbnail_" + time + ".jpg";
        Bitmap bitmap;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        //decode first with inJustDecodeBounds to get dimensions
        BitmapFactory.decodeFile(path, options);

        options.inPreferredConfig = Bitmap.Config.ALPHA_8;
        options.inSampleSize = calculateInSampleSize(options, 250, 250);
        options.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(path, options);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        File f = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                + File.separator + imgThumbnail);
        FileOutputStream fo = null;
        try {
            f.createNewFile();
            fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
            bytes.close();
            artVO.setThumbImagePath(f.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bitmap = null;

        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int inSampleSize = 15;
        final int height = options.outHeight;
        final int width = options.outWidth;

        if (height > reqHeight || width > reqWidth) {

            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    public static String getTime(Date date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        return dateFormat.format(date);
    }

    public static Bitmap calculateHeightWidth(Bitmap bitmap) {
        int idealWidth = 1920;
        int idealHeight = 1044;

        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();

        if (bitmapHeight > idealHeight || bitmapWidth > idealWidth) {
            return bitmap;
        } else {
            float widthFactor = bitmapWidth / idealWidth;
            float heightFactor = bitmapHeight / idealHeight;
            if (bitmapHeight / widthFactor < 1044) {
                bitmapHeight = (int) (bitmapHeight / widthFactor);
            } else {
                bitmapHeight = 1044;
            }

            if (bitmapWidth / heightFactor < 1920) {
                bitmapWidth = (int) (bitmapWidth / heightFactor);
            } else {
                bitmapWidth = 1920;
            }
        }

        return Bitmap.createScaledBitmap(bitmap, bitmapWidth, bitmapHeight,
                true);
    }

    public static boolean isExternalStrorageAvailable() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

}
