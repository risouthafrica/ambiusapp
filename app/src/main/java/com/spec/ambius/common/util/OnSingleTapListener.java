package com.spec.ambius.common.util;

/**
 * Created by kunalk on 11/26/2014.
 */
public interface OnSingleTapListener {
    void onLongPress(boolean isVisible);

    void onSingleTap(boolean isVisible);
}
