/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.spec.ambius.common.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.spec.ambius.R;
import com.spec.ambius.common.database.DatabaseManager;
import com.spec.ambius.common.util.MessageDialogFragment;
import com.spec.ambius.common.util.OnDialogActionListener;
import com.spec.ambius.view.MainActivity;
import com.spec.ambius.vo.ArtVO;

import java.io.File;
import java.util.ArrayList;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";

    private ArrayList<ArtVO> mArtVOs;
    private OnViewHolderClicks mViewHolderClicks;
    private Context mContext;
    private MessageDialogFragment messageDialogFragment;
    private FragmentManager fragmentManager;
    private DatabaseManager databaseManager;
    private int positionToRemove = -1;

    /**
     * Provide a reference to the type of views that you are using (custom viewholder)
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView imageView;
        private final ImageButton btnRemove;
        private final TextView imageName;

        public ViewHolder(View v, OnViewHolderClicks onViewHolderClicks) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.imgGalleryItem);
            btnRemove = (ImageButton) v.findViewById(R.id.btnRemoveArt);
            imageName = (TextView) v.findViewById(R.id.txtArtName);
            btnRemove.setOnClickListener(this);
            imageView.setOnClickListener(this);
            mViewHolderClicks = onViewHolderClicks;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public TextView getTextView() {
            return imageName;
        }

        @Override
        public void onClick(View v) {
            mViewHolderClicks.onClick(v, getPosition());
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     */
    public GalleryAdapter(Context context, ArrayList<ArtVO> artVOs) {
        mArtVOs = artVOs;
        mContext = context;
        fragmentManager = ((Activity) context).getFragmentManager();
        databaseManager = new DatabaseManager(context);
    }


    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int position) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_gallery_item, viewGroup, false);

        ViewHolder vh = new ViewHolder(v, new OnViewHolderClicks() {
            @Override
            public void onClick(View view, int mPosition) {
                if (view.getId() == R.id.imgGalleryItem) {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("artVO", mArtVOs.get(mPosition));
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                } else if (view.getId() == R.id.btnRemoveArt) {
                    positionToRemove = mPosition;
                    messageDialogFragment = null;
                    messageDialogFragment = MessageDialogFragment.newInstance(actionListener, mContext.getResources().getString(R.string.msg_remove_art), R.layout.layout_dialog_posneg);
                    messageDialogFragment.show(fragmentManager, "messageDialog");
                }
            }
        });
        return vh;
    }

    OnDialogActionListener actionListener = new OnDialogActionListener() {
        @Override
        public void onClick(int id, int secondId) {
            if (messageDialogFragment != null)
                messageDialogFragment.dismiss();
            if (id == R.id.btnYes) {
                ArtVO artVO = mArtVOs.get(positionToRemove);
                removeFile(artVO.getEditedImagePath());
                removeFile(artVO.getOriginalImagePath());
                removeFile(artVO.getThumbImagePath());
                databaseManager.deleteArt(artVO.getId());
                mArtVOs.remove(artVO);
                notifyDataSetChanged();
            }
        }
    };

    private void removeFile(String path) {
        File file = new File(path);
        file.delete();
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(mArtVOs.get(position).getThumbImagePath(), options);
        viewHolder.getImageView().setImageBitmap(bitmap);
        viewHolder.getTextView().setText(mArtVOs.get(position).getEditedImagePath().substring(mArtVOs.get(position).getEditedImagePath().lastIndexOf("/") + 1));
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mArtVOs.size();
    }

    public interface OnViewHolderClicks {
        void onClick(View view, int mPosition);
    }
}
