package com.spec.ambius.common.util;

import android.app.DialogFragment;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.spec.ambius.R;


public class DialogListFragment extends DialogFragment {
	
	private OnDialogActionListener actionListener;
	private String[] items;
	private TypedArray ids;
	private String title;
	
    public static DialogListFragment newInstance(OnDialogActionListener actionListener, String[] items, TypedArray ids, String title) {
    	DialogListFragment frag = new DialogListFragment();
    	frag.actionListener = actionListener;
    	frag.items = items;
    	frag.ids = ids;  
    	frag.title = title;
        return frag;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.layout_dialog, container, false);
    	getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    	TextView txtTitle = (TextView)view.findViewById(R.id.txtTitle);
    	txtTitle.setText(title);
    	LinearLayout lnrDialog = (LinearLayout)view.findViewById(R.id.rltDialogOption);
    	
    	LayoutParams textParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    	LayoutParams lineParams = new LayoutParams(LayoutParams.MATCH_PARENT, (int)getActivity().getResources().getDimension(R.dimen.dialog_sep_height));
    	int padding = (int)getActivity().getResources().getDimension(R.dimen.dialog_item_margin);
    	
    	for (int i = 0; i < items.length ; i++) {
    		TextView textView = new TextView(getActivity());
    		textView.setLayoutParams(textParams);
    		textView.setGravity(Gravity.LEFT);
    		textView.setBackgroundColor(Color.WHITE);
    		textView.setPadding(padding, padding, 0, padding);
    		textView.setText(items[i]);
    		textView.setTextColor(Color.BLACK);
    		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
    		textView.setId(ids.getResourceId(i, 0));
    		/*Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
    		textView.setTypeface(typeface);*/
    		
    		if(i==0){
    			lnrDialog.addView(textView);    			
    		}else{
    			View line = new View(getActivity());
    			line.setLayoutParams(lineParams);
    			line.setBackgroundResource(R.drawable.popup_seprator);
    			lnrDialog.addView(line);
    			lnrDialog.addView(textView);    			
    		}
    		
    		textView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					actionListener.onClick(v.getId(), 0);
				}
			});
		}
    	    	
    	return view;
    }
}
