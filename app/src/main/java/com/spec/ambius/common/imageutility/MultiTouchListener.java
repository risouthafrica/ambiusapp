package com.spec.ambius.common.imageutility;

import android.content.Context;
import android.graphics.Matrix;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.spec.ambius.R;
import com.spec.ambius.common.constants.Global;
import com.spec.ambius.common.util.OnSingleTapListener;
import com.spec.ambius.view.MainActivity;

import java.util.Set;

public class MultiTouchListener implements OnTouchListener {

    private static String TAG = "MultiTouchListener";

    private static final int INVALID_POINTER_ID = -1;
    public boolean isRotateEnabled = true;
    public boolean isTranslateEnabled = true;
    public boolean isScaleEnabled = true;
    public float minimumScale = 0.1f;
    public float maximumScale = 10.0f;
    private int mActivePointerId = INVALID_POINTER_ID;
    private float mPrevX;
    private float mPrevY;
    private ScaleGestureDetector mScaleGestureDetector;
    private GestureDetector gestureDetector;
    private OnSingleTapListener singleTapListener;
    private boolean isVisible;

    private CollageView bgCollageView;
    private static Context mContext;

    private int scrollByY, scrollByX;
    private float dx, dy;
    private int sdk;
    private static Set<CollageView> iteamGroupSet;
    Matrix matrix = new Matrix();

  /*  private int maxLeft, maxRight, maxTop, maxBottom, totalX, totalY;
    private float globalX, globalY;*/

    public MultiTouchListener(Context context, ImageView imageView, Set iteamGroupSet) {
        mScaleGestureDetector = new ScaleGestureDetector(new ScaleGestureListener());
        gestureDetector = new GestureDetector(context, new SingleTapConfirm());
        mContext = context;
        sdk = android.os.Build.VERSION.SDK_INT;
        this.iteamGroupSet = iteamGroupSet;

 /*       maxLeft = bound[0];
        maxRight = bound[1];
        maxBottom = bound[2];
        maxTop = bound[3];*//*
        float[] values = new float[9];
        imageView.getMatrix().getValues(values);
        globalX = values[Matrix.MTRANS_X];
        globalY = values[Matrix.MTRANS_Y];

        //Log.d(TAG, imageView.getDrawable().getIntrinsicWidth() + "getIntrinsicWidth : getIntrinsicHeight " + imageView.getDrawable().getIntrinsicHeight());
        Log.d(TAG, maxLeft + " : " + maxRight + " : " + maxBottom + " : " + maxTop);
        Log.d(TAG, globalX + "  Matrix  " + globalY + " : " + values[0] * imageView.getWidth());*/

        Log.d(TAG, imageView.getLeft() + " : " + imageView.getRight() + " : " + imageView.getBottom() + " : " + imageView.getTop());

    }

    private static float adjustAngle(float degrees) {
        if (degrees > 180.0f) {
            degrees -= 360.0f;
        } else if (degrees < -180.0f) {
            degrees += 360.0f;
        }

        return degrees;
    }

    private static void move(View view, TransformInfo info) {
        computeRenderOffset(view, info.pivotX, info.pivotY);
        adjustTranslation(view, info.deltaX, info.deltaY);

        // Assume that scaling still maintains aspect ratio.
        float scale = view.getScaleX() * info.deltaScale;
        scale = Math.max(info.minimumScale, Math.min(info.maximumScale, scale));
        view.setScaleX(scale);
        view.setScaleY(scale);

//        Global.targetView.setScaleX(scale);
//        Global.targetView.setScaleY(scale);

        float rotation = adjustAngle(view.getRotation() + info.deltaAngle);
        view.setRotation(rotation);
    }

    private static void adjustTranslation(View view, float deltaX, float deltaY) {
        float[] deltaVector = {deltaX, deltaY};
        // view.getMatrix().mapVectors(deltaVector);

      /*  view.setTranslationX(view.getTranslationX() + deltaVector[0]);
        view.setTranslationY(view.getTranslationY() + deltaVector[1]);*/

        if (Global.isGroupCreated) {
            Log.d(TAG, "Group Size :::::::" + iteamGroupSet.size());


            if (iteamGroupSet.size() > 0 && Global.isGroupCreated) {
                /*Global.targetView.getMatrix().mapVectors(deltaVector);
                Global.targetView.setTranslationX(view.getTranslationX() + deltaVector[0]);
                Global.targetView.setTranslationY(view.getTranslationY() + deltaVector[1]);*/
                view.getMatrix().mapVectors(deltaVector);
                for (CollageView collageView : iteamGroupSet) {
                    // collageView.getMatrix().mapVectors(deltaVector);
/*                    Log.d(TAG, "Before TransitionX : " + "Id :" + collageView.getId()+" " + "  X :" + collageView.getTranslationX());
                    Log.d(TAG, "Before TransitionY : " + "Id :" + collageView.getId() + " " + "  Y :" + collageView.getTranslationY());
                    Log.d(TAG, "Before NormalX : " + "Id :" + collageView.getId() + "  X :" + collageView.getX());
                    Log.d(TAG, "Before NormalY : " + "Id :" + collageView.getId() + "  Y :" + collageView.getY());
                    Log.d(TAG, "Delta Vector [0] : " + deltaVector[0]);
                    Log.d(TAG, "Delta Vector [1] : " + deltaVector[1]);*/
                    //collageView.getMatrix().mapVectors(deltaVector);

                    // if(!collageView.equals(Global.targetView)){
                    // collageView.getMatrix().mapVectors(deltaVector);
                    //collageView.getMatrix().mapVectors(deltaVector);
                    collageView.setTranslationX(collageView.getTranslationX() + deltaVector[0]);
                    collageView.setTranslationY(collageView.getTranslationY() + deltaVector[1]);


                    //}

 /*                   Log.d(TAG, "After TransitionX : " + "Id :" + collageView.getId()+" " + "  X :" + collageView.getTranslationX());
                    Log.d(TAG, "After TransitionY : " + "Id :" + collageView.getId()+ " " + "  Y :" + collageView.getTranslationY());
                    Log.d(TAG, "After NormalX : " + "Id :" + collageView.getId()+ " " + "  X :" + collageView.getX());
                    Log.d(TAG, "After NormalY : " + "Id :" + collageView.getId()+" " + "  Y :" + collageView.getY());
                    Log.d(TAG, "=======================================================================================================");*/
                }
            }
        } else {
           /* Log.d(TAG, "Before adjustTranslation : " + "Id :" + view.getId() + "  X :" + view.getTranslationX());
            Log.d(TAG, "Before adjustTranslation : " + "Id :" + view.getId() + "  Y :" + view.getTranslationY());
            Log.d(TAG, "Before adjustTranslation : " + "Id :" + view.getId() + "  X :" + view.getX());
            Log.d(TAG, "Before adjustTranslation : " + "Id :" + view.getId() + "  Y :" + view.getY());
            Log.d(TAG, "Delta Vector [0] : " + deltaVector[0]);
            Log.d(TAG, "Delta Vector [1] : " + deltaVector[1]);*/
            view.getMatrix().mapVectors(deltaVector);
            view.setTranslationX(view.getTranslationX() + deltaVector[0]);
            view.setTranslationY(view.getTranslationY() + deltaVector[1]);
          /*  Log.d(TAG, "After adjustTranslation : " + "Id :" + view.getId() + "  X :" + view.getTranslationX());
            Log.d(TAG, "After adjustTranslation : " + "Id :" + view.getId() + "  Y :" + view.getTranslationY());
            Log.d(TAG, "After adjustTranslation : " + "Id :" + view.getId() + "  X :" + view.getX());
            Log.d(TAG, "After adjustTranslation : " + "Id :" + view.getId() + "  Y :" + view.getY());*/
        }

    }

    private static void computeRenderOffset(View view, float pivotX, float pivotY) {
        if (view.getPivotX() == pivotX && view.getPivotY() == pivotY) {
            return;
        }

        float[] prevPoint = {0.0f, 0.0f};
        view.getMatrix().mapPoints(prevPoint);

        view.setPivotX(pivotX);
        view.setPivotY(pivotY);

        float[] currPoint = {0.0f, 0.0f};
        view.getMatrix().mapPoints(currPoint);

        float offsetX = currPoint[0] - prevPoint[0];
        float offsetY = currPoint[1] - prevPoint[1];

        view.setTranslationX(view.getTranslationX() - offsetX);
        view.setTranslationY(view.getTranslationY() - offsetY);
    }

    public void setSingleTapListener(OnSingleTapListener singleTapListener, boolean isVisible) {
        this.singleTapListener = singleTapListener;
        this.isVisible = isVisible;
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
/*
        @Override
        public boolean onLongPress(MotionEvent e) {
            return true;
        }*/

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
            singleTapListener.onLongPress(true);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        mScaleGestureDetector.onTouchEvent(view, event);
        Log.d(TAG, "View : " + view.getWidth() + " : " + view.getHeight());

        int imageWidth = view.getWidth();
        int imageHeight = view.getHeight();


        if (!isTranslateEnabled) {
            return true;
        }

        gestureDetector.onTouchEvent(event);

        int action = event.getAction();
        switch (action & event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN: {
                System.out.println("Action down called");
                if (!Global.isGroupCreated) {
                    if (Global.targetView != null && !Global.targetView.equals(view)) {
                        Global.targetView.findViewById(R.id.btnClose).setVisibility(View.GONE);
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            Global.targetView.setBackgroundResource(mContext.getResources().getColor(android.R.color.transparent));
                        } else {
                            Global.targetView.setBackground(null);
                        }
                        Global.targetView = null;
                    } else if (Global.targetView != null) {
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            Global.targetView.setBackgroundResource(mContext.getResources().getColor(android.R.color.transparent));
                        } else {
                            Global.targetView.setBackground(null);
                        }
                    }
                } else {
                    /*if(Global.isGroupCreated){

                        for(CollageView collageView:iteamGroupSet){
                            if(!collageView.equals(Global.targetView))
                                   collageView.setOnTouchListener(null);
                        }
                        Global.targetView.setOnTouchListener(this);
                    }
*/
                }
                if (Global.isGroupCreated) {
                    for (CollageView collageView : iteamGroupSet) {
                        if (!collageView.equals(Global.targetView)) {
                            collageView.setClickable(false);
                            collageView.setFocusable(false);
                            collageView.setFocusableInTouchMode(false);
                        }
                    }
                    Global.targetView.setOnTouchListener(MainActivity.getMultiTouchListener());
                }

                Global.targetView = (CollageView) view;

                mPrevX = event.getX();
                mPrevY = event.getY();

               /* Log.d(TAG, "ontouch down : " + "mPrevX :" + mPrevX);
                Log.d(TAG, "ontouch down : " + "mPrevY :" + mPrevY);

                Log.d(TAG, "ontouch down : " +"Id :"+Global.targetView.getId() +"current view X :" + Global.targetView.getX());
                Log.d(TAG, "ontouch down : " +"Id :"+Global.targetView.getId() +"current view Y :" + Global.targetView.getY());

                Log.d(TAG, "Action down : " + mPrevX + " : " + mPrevY);*/

                // Save the ID of this pointer.
                mActivePointerId = event.getPointerId(0);

                break;
            }

            case MotionEvent.ACTION_MOVE: {
                Log.d(TAG, " width -measure_width= " + (view.getWidth() - view.getMeasuredWidth()) +
                        " height -measure_height= " + (view.getHeight() - view.getMeasuredHeight()));
                System.out.println("Action move called");
                if (Global.targetView != null && !Global.isGroupCreated) {
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        Global.targetView.setBackgroundResource(mContext.getResources().getColor(android.R.color.transparent));
                    } else {
                        Global.targetView.setBackground(null);
                    }
                }
                Log.d(TAG, "ontouch : " + view.getTop() + " : " + view.getLeft() + " : " + view.getRight() + " : " + view.getBottom());
                // Find the index of the active pointer and fetch its position.
                int pointerIndex = event.findPointerIndex(mActivePointerId);
                if (pointerIndex != -1) {
                    float currentX = event.getX(pointerIndex);
                    float currentY = event.getY(pointerIndex);

                    float rawX = event.getRawX();
                    float rawY = event.getRawY();

                    scrollByX = (int) (currentX - mPrevX);
                    scrollByY = (int) (currentY - mPrevY);

                /*    Log.d(TAG, "ontouch move : " + "currentX :" + currentX);
                    Log.d(TAG, "ontouch move : " + "currentY :" + currentY);

                    Log.d(TAG, "ontouch move : " + "rawX :" + rawX);
                    Log.d(TAG, "ontouch move : " + "rawY :" + rawY);

                    Log.d(TAG, "ontouch move : " + "scrollByX :" + scrollByX);
                    Log.d(TAG, "ontouch move : " + "scrollByY :" + scrollByY);

                    Log.d(TAG, "ontouch move : " + "mPrevX :" + mPrevX);
                    Log.d(TAG, "ontouch move : " + "mPrevY :" + mPrevY);

                    Log.d(TAG, "ontouch move : "+"Id :"+Global.targetView.getId() + "current view X :" + Global.targetView.getX());
                    Log.d(TAG, "ontouch move : "+"ID :"+Global.targetView.getId() +  "current view Y :" + Global.targetView.getY());
                    Log.d(TAG, "=======================================================================================================");*/
                    // Only move if the ScaleGestureDetector isn't processing a
                    // gesture.
                    if (!mScaleGestureDetector.isInProgress()) {
                        Log.d(TAG, "RAW X : " + rawX + " RAW Y : " + rawY);
                       /* if (view.getLeft() + mPrevX > maxLeft && view.getLeft() + imageWidth - mPrevX < maxRight
                                && view.getTop() + mPrevY > maxTop && view.getBottom() + imageHeight - mPrevY < maxBottom)
                            adjustTranslation(view, scrollByX, scrollByY);*/

                       /* if (view.getLeft() + mPrevX > maxLeft && view.getLeft() + imageWidth - mPrevX < maxRight
                                && view.getTop() + mPrevY > maxTop && view.getTop() + imageHeight - mPrevY < maxBottom)
                            adjustTranslation(view, scrollByX, scrollByY);*/

                        adjustTranslation(view, scrollByX, scrollByY);

                    }
                    break;
                }
            }
            case MotionEvent.ACTION_CANCEL:
                mActivePointerId = INVALID_POINTER_ID;
                break;

            case MotionEvent.ACTION_UP:

                System.out.println("Action up called");

              /*  Log.d(TAG, "ontouch up : "+ "Id :"+ Global.targetView.getId() + "current view X :" + Global.targetView.getX());
                Log.d(TAG, "ontouch up : "+ "Id :"+ Global.targetView.getId() + "current view Y :" + Global.targetView.getY());*/
                if (Global.isGroupCreated) {
                    for (CollageView collageView : iteamGroupSet) {
                        collageView.setFocusable(true);
                        collageView.setClickable(true);
                        collageView.setFocusableInTouchMode(true);
                    }
                }
                if (!Global.isGroupCreated) {
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        Global.targetView.setBackgroundResource(R.drawable.border);
                    } else {
                        Global.targetView.setBackground(mContext.getResources().getDrawable(R.drawable.border));
                    }
                }

                /*Log.d(TAG, "ontouch up : " + "event X :" + event.getX());
                Log.d(TAG, "ontouch up : " + "event Y :" + event.getY());

                Log.d(TAG, "ontouch up : " + "prevX :" + mPrevX);
                Log.d(TAG, "ontouch up : " + "prevY :" + mPrevY);*/
                float minX = event.getX() - 10;
                float maxX = event.getX() + 10;
                float minY = event.getY() - 10;
                float maxY = event.getY() + 10;


                //if (mPrevX == event.getX() && mPrevY == event.getY()) {
                if (mPrevX > minX && mPrevX < maxX && mPrevY > minY && mPrevY < maxY) {
                    if (Global.targetView != null) {

                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            Global.targetView.setBackgroundResource(R.drawable.border);
                        } else {
                            Global.targetView.setBackground(mContext.getResources().getDrawable(R.drawable.border));
                        }
                    }
                    singleTapListener.onSingleTap(isVisible);
                }
                mActivePointerId = INVALID_POINTER_ID;
                break;

            case MotionEvent.ACTION_POINTER_UP: {
                System.out.println("Action ponter up called");
                if (Global.targetView != null) {
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        Global.targetView.setBackgroundResource(R.drawable.border);
                    } else {
                        Global.targetView.setBackground(mContext.getResources().getDrawable(R.drawable.border));
                    }
                }
                // Extract the index of the pointer that left the touch sensor.
                int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                int pointerId = event.getPointerId(pointerIndex);
                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mPrevX = event.getX(newPointerIndex);
                    mPrevY = event.getY(newPointerIndex);
                   /* Log.d(TAG, "ontouch ACTION_POINTER_UP : " + "mPrevX :" + mPrevX);
                    Log.d(TAG, "ontouch ACTION_POINTER_UP : " + "mPrevY :" + mPrevY);

                    Log.d(TAG, "ontouch ACTION_POINTER_UP : " + "Id :"+ Global.targetView.getId() + "current view X :" + Global.targetView.getX());
                    Log.d(TAG, "ontouch ACTION_POINTER_UP : " + "Id :"+ Global.targetView.getId() +  "current view Y :" + Global.targetView.getY());*/
                    mActivePointerId = event.getPointerId(newPointerIndex);

                }

                break;
            }
        }
        /*bgCollageView.onTouchEvent(event);*/
        return true;
    }

    public CollageView getBgCollageView() {
        return bgCollageView;
    }

    public void setBgCollageView(CollageView bgCollageView) {
        this.bgCollageView = bgCollageView;
    }

    private class ScaleGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        private float mPivotX;
        private float mPivotY;
        private Vector2D mPrevSpanVector = new Vector2D();

        @Override
        public boolean onScaleBegin(View view, ScaleGestureDetector detector) {
            if (!Global.isGroupCreated) {
                mPivotX = detector.getFocusX();
                mPivotY = detector.getFocusY();
                mPrevSpanVector.set(detector.getCurrentSpanVector());
                return true;
            }

            return false;
        }

        @Override
        public boolean onScale(View view, ScaleGestureDetector detector) {
            if (!Global.isGroupCreated) {
                if (Global.targetView != null) {
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        Global.targetView.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
                    } else {
                        Global.targetView.setBackground(null);
                    }
                }
                TransformInfo info = new TransformInfo();
                info.deltaScale = isScaleEnabled ? detector.getScaleFactor() : 1.0f;
                info.deltaAngle = isRotateEnabled ? Vector2D.getAngle(mPrevSpanVector, detector.getCurrentSpanVector()) : 0.0f;
                info.deltaX = isTranslateEnabled ? detector.getFocusX() - mPivotX : 0.0f;
                info.deltaY = isTranslateEnabled ? detector.getFocusY() - mPivotY : 0.0f;
                info.pivotX = mPivotX;
                info.pivotY = mPivotY;
                info.minimumScale = minimumScale;
                info.maximumScale = maximumScale;
                move(view, info);
            }

            return false;
        }

        @Override
        public void onScaleEnd(View view, ScaleGestureDetector detector) {
            super.onScaleEnd(view, detector);

            Log.d(TAG, " width -measure_width= " + (view.getWidth() - view.getMeasuredWidth()) +
                    " height -measure_height= " + (view.getHeight() - view.getMeasuredHeight()));
        }
    }

    private class TransformInfo {

        public float deltaX;
        public float deltaY;
        public float deltaScale;
        public float deltaAngle;
        public float pivotX;
        public float pivotY;
        public float minimumScale;
        public float maximumScale;
    }
}