package com.spec.ambius.common.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.spec.ambius.common.constants.Constants;


/**
 * Created by kunalk on 12/3/2014.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "rentokil.db";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE_ART = "create table "
            + Constants.TABLE_ART + "(" + Constants.COLUMN_ID + " integer primary key autoincrement, "
            + Constants.COLUMN_IMG_PATH_EDITED + " text not null, "
            + Constants.COLUMN_IMG_PATH_ORIGINAL + " text not null, "
            + Constants.COLUMN_IMG_PATH_THUMB + " text not null"
            + ");";

  /*  private static final String CREATE_COLOR_TABLE = "create table "
            + Constants.TABLE_COLOR + "(" + Constants.COLOR_ID + " integer primary key autoincrement, "
            + Constants.COLOR_NAME + " text not null, "
            + Constants.COLOR_CODE + " text not null"
            + ");";*/

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ART);
        //db.execSQL(CREATE_COLOR_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
