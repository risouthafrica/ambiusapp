package com.spec.ambius.common.util;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.spec.ambius.R;


public class MessageDialogFragment extends DialogFragment implements View.OnClickListener{
	
	private OnDialogActionListener actionListener;
	private String message;
	private int layout;
	
    public static MessageDialogFragment newInstance(OnDialogActionListener actionListener, String message, int layout) {
    	MessageDialogFragment frag = new MessageDialogFragment();
    	frag.actionListener = actionListener;
    	frag.message = message;
    	frag.layout = layout;
        return frag;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	View view = inflater.inflate(layout, container, false);    	
    	getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);    	
    	
    	TextView txtMessage = (TextView)view.findViewById(R.id.txtMessage);
    	txtMessage.setText(message);
        Button btnOk = (Button)view.findViewById(R.id.btnYes);
        Button btnCancel = (Button)view.findViewById(R.id.btnNo);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    	return view;
    }

    @Override
    public void onClick(View v) {
        actionListener.onClick(v.getId(),0);
    }
}
