package com.spec.ambius.common.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.spec.ambius.R;
import com.spec.ambius.vo.ColorVO;

import java.util.ArrayList;

/** An array adapter that knows how to render views when given CustomData classes */
public class CustomArrayAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<ColorVO> colorVOs;
    private Context mContext;

    public CustomArrayAdapter(Context context, ArrayList<ColorVO> colorVOs) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.colorVOs = colorVOs;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return colorVOs.size();
    }

    @Override
    public Object getItem(int position) {
        return colorVOs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) {
            // Inflate the view since it does not exist
            convertView = mInflater.inflate(R.layout.layout_color, parent, false);

            // Create and save off the holder in the tag so we get quick access to inner fields
            // This must be done for performance reasons
            holder = new Holder();
            holder.imgButton = (ImageButton) convertView.findViewById(R.id.imgColor);
            holder.txtColorName = (TextView) convertView.findViewById(R.id.txtColorName);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        Drawable drawable = mContext.getResources().getDrawable(R.drawable.yello_circle);
        ColorVO colorVO = (ColorVO)getItem(position);
        drawable.setColorFilter(Color.parseColor(colorVO.getColorCode()), PorterDuff.Mode.SRC_ATOP);

        holder.imgButton.setImageDrawable(drawable);
        holder.txtColorName.setText(colorVO.getColorName());
        return convertView;
    }

    /** View holder for the views we need access to */
    private static class Holder {
        public ImageButton imgButton;
        public TextView txtColorName;
    }
}
