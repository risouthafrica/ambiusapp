package com.spec.ambius.common.parser;

import android.content.Context;

import com.spec.ambius.vo.ColorVO;
import com.spec.ambius.vo.ItemVO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by umangk on 26-Dec-14.
 */
public class CatalogueJsonParser {

    private static int lastCount = 0;
    private static String categoryName = null;

    public static void getAllItemsFromJson(Context context, ArrayList<ItemVO> itemVOList) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("Catalogue.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = (JSONObject) jsonArray.get(i);
                itemVOList.add(new ItemVO(item.getString("name"), item.getBoolean("isItem"), item.getInt("id"), item.getInt("parentId"), item.optString("drawable_name"), item.optInt("colorId")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getAllSpecificItems(ArrayList<ItemVO> itemVOList, ArrayList<ItemVO> categoryList, int parentId) {
        try {
            for (ItemVO item : itemVOList) {
                if (item.getParentId() == parentId) {
                    categoryList.add(item);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getAllColors(ArrayList<ColorVO> colorVos, Context context) {
        String colorJson = null;
        try {
            InputStream is = context.getAssets().open("Colors.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            colorJson = new String(buffer, "UTF-8");
            JSONObject jsonObject = new JSONObject(colorJson);
            JSONArray jsonArray = jsonObject.getJSONArray("colors");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject colorObj = (JSONObject) jsonArray.get(i);
                colorVos.add(new ColorVO(colorObj.getInt("color_id"), colorObj.getString("color_name"), colorObj.getString("color_code")));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getTopParent(Context context, ItemVO item) {
        String json = null;
        ArrayList<ItemVO> itemVOList = new ArrayList<>();
        try {
            InputStream is = context.getAssets().open("Catalogue.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject itemJson = (JSONObject) jsonArray.get(i);
                itemVOList.add(new ItemVO(itemJson.getString("name"), itemJson.getBoolean("isItem"), itemJson.getInt("id"), itemJson.getInt("parentId"), itemJson.optString("drawable_name"), itemJson.optInt("colorId")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getParent(itemVOList, item);
        return lastCount;
    }

    public static String getCategoryName(){
        return  categoryName;
    }

    private static void getParent(ArrayList<ItemVO> itemVOList, ItemVO itemVO) {
        if (itemVO.getParentId() == -1) {
            lastCount = itemVO.getItemId();
            categoryName = itemVO.getItemName();
        } else {
            for (ItemVO item : itemVOList) {
                if (itemVO.getParentId() == item.getItemId()) {
                    getParent(itemVOList, item);
                    break;
                }
            }
        }
    }
}
/*
if (itemVO.getParentId() == item.getItemId()) {
        if (itemVO.getParentId() == -1) {
        return itemVO.getItemId();
        } else if (itemVO.getParentId() == item.getItemId()) {
        getParent(itemVOList, item);
        }
        }*/
