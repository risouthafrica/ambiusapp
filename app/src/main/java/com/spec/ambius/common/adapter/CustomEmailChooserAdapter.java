package com.spec.ambius.common.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spec.ambius.R;

import java.util.ArrayList;

public class CustomEmailChooserAdapter extends BaseAdapter {

	private ArrayList<String> applicationNamesList;
	private ArrayList<Drawable> iconsList;
	private Context context;
	
	public CustomEmailChooserAdapter(Context context, ArrayList<String> applicationNamesList, ArrayList<Drawable> iconsList){
		this.context = context;
		this.applicationNamesList = applicationNamesList;
		this.iconsList = iconsList;
	}
	
	@Override
	public int getCount() {
		
		return applicationNamesList.size();
	}

	@Override
	public Object getItem(int position) {
		
		return applicationNamesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView applicationIcon;
		TextView applicationName;
		if(convertView == null){
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.layout_email_chooser, parent, false);
		}
		
		applicationIcon = (ImageView)convertView.findViewById(R.id.iconImageView);
		applicationName = (TextView)convertView.findViewById(R.id.applicationName);
		
		applicationName.setText(applicationNamesList.get(position));
		applicationIcon.setImageDrawable(iconsList.get(position));
		
		return convertView;
	}

}
