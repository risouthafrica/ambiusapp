package com.spec.ambius.common.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;
import com.spec.ambius.common.callbackinterface.IOnUserRecoverableException;
import com.spec.ambius.common.constants.Constants;
import com.spec.ambius.view.CreateArtActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

;

/**
 * Created by namrata on 1/7/2015.
 * GoogleSheetUtility is used to insert a record into a specific worksheet of a spreadsheet
 */
public class GoogleSheetUtility {

    private List<SpreadsheetEntry> spreadsheetEntryList;
    private List<WorksheetEntry> worksheetEntryList;

    private SpreadsheetService spreadsheetService;
    private WorksheetFeed worksheetFeed;

    private Activity activity;
    public IOnUserRecoverableException iOnUserRecoverableException;

    private SpreadsheetEntry requiredSpreadsheetEntry;
    private WorksheetEntry requiredWorksheetEntry;

    private String applicationName;
    private String url;
    private String accessToken = null;
    private ProgressDialog progressDialog;

    private SharedPreferences sharedpreferences;
    private  HashMap<String,ArrayList<String>> categoryItemMap;
    private ArrayList<String> iteams;
    private String accountName;
    private String targetEmail;
    private Handler handler;
    private Message message = new Message();

    /* @constructor GoogleSheetUtility
    * @param IOnUserRecoverableException
    * This constructor is used for instantiating GoogleSheetUtility.
    * Param IOnUserRecoverableException is an interface whose method is used as a callback to be invoked to ask for the permission
    * from the user only when the application is installed first time in the device*/
    public GoogleSheetUtility(IOnUserRecoverableException iOnUserRecoverableException, String applicationName, String url, Activity activity){
        this.iOnUserRecoverableException = iOnUserRecoverableException;
        this.activity = activity;
        this.applicationName = applicationName;

        this.url = url;
    }

    /*@method getAuthenticationToken
    * @param Activity, String
    * @return String
    * This method is used to generate authentication token for accessing google docs and drive of user
    * Param Activity is used as a context, String is used to pass accountName or emailId of which we need to get auth token for
    * Returns Authentication token in  the form of String*/
    public String getAuthenticationToken(Activity activity, String accountName){
        //Scopes used to get access to google docs and spreadsheets present in the drive
        String SCOPE1 = "https://spreadsheets.google.com/feeds";
        String SCOPE2 = "https://docs.google.com/feeds";
        String scope = "oauth2:" + SCOPE1 + " " + SCOPE2;
        String authenticationToken = null;
        try {
            authenticationToken = GoogleAuthUtil.getToken(activity, accountName, scope);
        }
        catch (UserRecoverableAuthException exception){
            if(progressDialog != null && progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            iOnUserRecoverableException.onUserRecoverableException(exception);
        }catch (IOException e) {
            e.printStackTrace();
            if(progressDialog != null && progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        } catch (GoogleAuthException e) {
            if(progressDialog != null && progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            e.printStackTrace();
        }
        return authenticationToken;
    }

    public void authenticateUser(String accountName){
        this.accountName = accountName;
        String[] params = {url, accountName};
        LoginAsyncTask loginAsyncTask = new LoginAsyncTask();
        loginAsyncTask.execute(params);


    }

    /*@class GetSpreadSheetFeedAsyncTask
    * This asynctask class is used to perform network operations off the user interface thread.
    * In this class, all spreadsheets associated with an email account is retrieved then a specific one is selected depending
    * upon the requirements then the required worksheet associated with that spreadsheet is selected and then the entry is inserted
    * into the worksheet. It displays a message "Row inserted successfully " if the operation is successful*/
    private class LoginAsyncTask extends AsyncTask<String, Void, Void>{

        String url;
        boolean isAllow = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(activity, "", "Authenticating...");
        }

        @Override
        protected Void doInBackground(String... params) {
            if (params != null && params.length > 0){
                try {
                    url = params[0];
                    accountName = params[1];
                    if (url != null && !url.equals("") && accountName != null && !accountName.equals("")){
                        URL spreadSheetUrl = new URL(url);
                            accessToken = getAuthenticationToken(activity, accountName);
                            if (accessToken != null && !accessToken.equals("")){
                                if (validateAuthTokenUtil(accessToken)){

                                }else {
                                    //calling this method in a condition when accesstoken is non null and non empty but has expired
                                    authenticateUser(accountName);
                                }
                               }

                    }

                }catch (MalformedURLException e){
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{
                if(progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                spreadsheetService = null;

                if(accessToken != null){
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                           // Toast.makeText(activity, "Signed in successfully", Toast.LENGTH_SHORT).show();
                            sharedpreferences = activity.getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(Constants.USER_EMAIL_PREF, accountName);
                            editor.commit();

                            Intent authenticateResultActivityIntent = new Intent(activity, CreateArtActivity.class);
                            authenticateResultActivityIntent.putExtra(Constants.IS_NEED_TO_INSERT,true);
           /*     authenticateResultActivityIntent.putExtra(Constants.DISPLAY_NAME, displayName);
                authenticateResultActivityIntent.putExtra(Constants.USER_ID, userId);*/
                            activity.startActivity(authenticateResultActivityIntent);
                            activity.finish();

                        }
                    });

                }else{
                  activity.runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                          Toast.makeText(activity, "Please try again", Toast.LENGTH_SHORT).show();
                      }
                  });
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }



    /*@method insertSpreadSheetEntries
    * @param String
    * @return void
    * This method is used to insert an entry into the worksheet of a specific spreadsheet. It returns nothing.
    * Param String is used to pass accountName or emailId. An entry is to be inserted into the worksheet of a specific spreadsheet
    * associated with this account*/
    public void insertLoginSpreadSheetEntries(String accountName,Handler handler){
        this.handler = handler;
        spreadsheetEntryList = new ArrayList<>();
        spreadsheetService = new SpreadsheetService(applicationName);
        spreadsheetService.setProtocolVersion(SpreadsheetService.Versions.V3);
        String[] params = {url, accountName};
        // Make a request to the API and get all spreadsheets.
        LoginLogInsertionAsyncTask loginLogInsertionAsyncTask = new LoginLogInsertionAsyncTask();
        loginLogInsertionAsyncTask.execute(params);
    }

    /*@class GetSpreadSheetFeedAsyncTask
    * This asynctask class is used to perform network operations off the user interface thread.
    * In this class, all spreadsheets associated with an email account is retrieved then a specific one is selected depending
    * upon the requirements then the required worksheet associated with that spreadsheet is selected and then the entry is inserted
    * into the worksheet. It displays a message "Row inserted successfully " if the operation is successful*/
    private class LoginLogInsertionAsyncTask extends AsyncTask<String, Void, Void>{

        String url;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(activity, "","Please wait...");
        }

        @Override
        protected Void doInBackground(String... params) {
            if (params != null && params.length > 0){
                try {
                    url = params[0];
                    accountName = params[1];
                    if (spreadsheetService != null && url != null && !url.equals("") && accountName != null && !accountName.equals("")){
                        URL spreadSheetUrl = new URL(url);
                        if(Util.getConnectivityStatusString(activity) != Constants.TYPE_NOT_CONNECTED_STRING){
                             accessToken = getAuthenticationToken(activity, accountName);
                            if (accessToken != null && !accessToken.equals("")){
                                if (validateAuthTokenUtil(accessToken)){
                                    Log.i(GoogleSheetUtility.class.getSimpleName(), accessToken);
                                    spreadsheetService.setAuthSubToken(accessToken);
                                    // Make a request to the API and get all spreadsheets.
                                    SpreadsheetFeed spreadsheetFeed = spreadsheetService.getFeed(spreadSheetUrl, SpreadsheetFeed.class);
                                    if (spreadsheetFeed != null){
                                        spreadsheetEntryList = spreadsheetFeed.getEntries();
                                        if (spreadsheetEntryList != null && !spreadsheetEntryList.isEmpty()){

                                            for (SpreadsheetEntry spreadsheetEntry: spreadsheetEntryList){
                                                String title = spreadsheetEntry.getTitle().getPlainText();
                                              //  Util.showToast(activity, title);
                                                Log.i(GoogleSheetUtility.class.getSimpleName(), title);
                                                if (title != null && title.equals(Constants.REQUIRED_SPREADSHEET_TITLE)){
                                                    //  Choose a spreadsheet more intelligently based on your app's needs                                              // app's needs.
                                                    requiredSpreadsheetEntry = spreadsheetEntry;
                                                    break;
                                                }
                                            }
                                            if (requiredSpreadsheetEntry == null) {
                                                message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                            }else{
                                                if (requiredSpreadsheetEntry.getCanEdit()){
                                                    worksheetFeed = spreadsheetService.getFeed(requiredSpreadsheetEntry.getWorksheetFeedUrl(), WorksheetFeed.class);
                                                    if (worksheetFeed != null){
                                                        worksheetEntryList = worksheetFeed.getEntries();
                                                        if (worksheetEntryList != null && !worksheetEntryList.isEmpty()){
                                                            for (WorksheetEntry worksheetEntry: worksheetEntryList){
                                                                String title = worksheetEntry.getTitle().getPlainText();
                                                                Log.i(GoogleSheetUtility.class.getSimpleName(), title);
                                                                if (title != null && title.equals(Constants.REQUIRED_LOGIN_WORKSHEET_TITLE)) {
                                                                    //  Choose a worksheet more intelligently based on your app's needs.
                                                                    requiredWorksheetEntry = worksheetEntry;
                                                                    break;
                                                                }
                                                            }
                                                            if (requiredWorksheetEntry == null){
                                                                message.what = Constants.NO_SPREAD_TAB_AVAILABLE;
                                                            }else {
                                                                if (requiredWorksheetEntry.getCanEdit()){
                                                                    URL listFeedUrl = requiredWorksheetEntry.getListFeedUrl();
                                                                    // Fetch the list feed of the worksheet.
                                                                    Set<String> columnsSet = null;
                                                                    if(listFeedUrl != null){
                                                                        ListFeed listFeed = spreadsheetService.getFeed(listFeedUrl, ListFeed.class);
                                                                        if(listFeed != null && listFeed.getEntries().size() > 0){
                                                                            ListEntry entry = listFeed.getEntries().get(0);
                                                                            if(entry != null){
                                                                                columnsSet = entry.getCustomElements().getTags();
                                                                            }
                                                                        }
                                                                    }
                                                                    if(columnsSet != null && columnsSet.size() > 0 && !columnsSet.contains(Constants.CHECK_COLOUMN_USER_EMAIL)){
                                                                        message.what = Constants.NO_USER_EMAIL;
                                                                    }else if(columnsSet != null && columnsSet.size() > 0 && !columnsSet.contains(Constants.CHECK_COLOUMN_DATE_TIME)){
                                                                        message.what = Constants.NO_DATE_TIME;
                                                                    }else{
                                                                        // Create a local representation of the new row.
                                                                        ListEntry listEntry = new ListEntry();
                                                                        listEntry.getCustomElements().setValueLocal(Constants.COLUMN_USER_EMAIL, accountName);
                                                                        listEntry.getCustomElements().setValueLocal(Constants.COLUMN_DATE_TIME,Util.getCurrentDateTime());
                                                                        // Send the new row to the API for insertion.

                                                                        ListEntry resultListEntry = spreadsheetService.insert(listFeedUrl, listEntry);
                                                                        if (resultListEntry != null){
//                                                                        Util.showToast(activity, resultListEntry.getTitle().getPlainText());
//                                                                        Util.showToast(activity, Constants.ROW_INSERTED_SUCCESSFULLY);
                                                                        }
                                                                    }
                                                                }else{
                                                                    message.what = Constants.NO_EDIT_ACCESS;
                                                                }
                                                            }
                                                        }else{
                                                            message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                                        }
                                                    }else{
                                                        message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                                    }
                                                }else{
                                                    message.what = Constants.NO_EDIT_ACCESS;
                                                }
                                            }
                                        }else {
                                            message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                        }
                                    }else{
                                        message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                    }
                                }else {
                                    message.what = Constants.NO_AUTHORIZED;
                                }
                            }else {
                                //calling this method in a condition when accesstoken is non null and non empty but has expired
                                insertLoginSpreadSheetEntries(accountName,handler);
                            }
                        }else{
                            message.what = Constants.INTERNET_CONNECTION;
                        }
                    }

                }catch (MalformedURLException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }catch (ServiceException e){
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{
                if(progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                handler.sendMessage(message);
                spreadsheetService = null;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }





    /*@method insertSpreadSheetEntries
 * @param String
 * @return void
 * This method is used to insert an entry into the worksheet of a specific spreadsheet. It returns nothing.
 * Param String is used to pass accountName or emailId. An entry is to be inserted into the worksheet of a specific spreadsheet
 * associated with this account*/
    public void insertItemDetailSpreadSheetEntries(String accountName,String targetEmail,HashMap<String,ArrayList<String>> categoryItemMap,Handler handler){
        this.categoryItemMap = categoryItemMap;
        spreadsheetEntryList = new ArrayList<>();
        spreadsheetService = new SpreadsheetService(applicationName);
        spreadsheetService.setProtocolVersion(SpreadsheetService.Versions.V3);
        String[] params = {url, accountName,targetEmail};
        this.handler = handler;
        // Make a request to the API and get all spreadsheets.
        ItemDetailLogInsertionAsyncTask itemLogInsertionAsyncTask = new ItemDetailLogInsertionAsyncTask();
        itemLogInsertionAsyncTask.execute(params);
    }

    /*@class GetSpreadSheetFeedAsyncTask
    * This asynctask class is used to perform network operations off the user interface thread.
    * In this class, all spreadsheets associated with an email account is retrieved then a specific one is selected depending
    * upon the requirements then the required worksheet associated with that spreadsheet is selected and then the entry is inserted
    * into the worksheet. It displays a message "Row inserted successfully " if the operation is successful*/
    private class ItemDetailLogInsertionAsyncTask extends AsyncTask<String, Void, Void>{

        String url;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(activity, "", "Please wait...");
        }

        @Override
        protected Void doInBackground(String... params) {
            if (params != null && params.length > 0){

                try {
                    url = params[0];
                    accountName = params[1];
                    targetEmail = params[2];
                    if (spreadsheetService != null && url != null && !url.equals("") && accountName != null && !accountName.equals("")){
                        URL spreadSheetUrl = new URL(url);
                        if(Util.getConnectivityStatusString(activity) != Constants.TYPE_NOT_CONNECTED_STRING){
                            accessToken = getAuthenticationToken(activity, accountName);
                            if (accessToken != null && !accessToken.equals("")){
                                if (validateAuthTokenUtil(accessToken)){
                                    Log.i(GoogleSheetUtility.class.getSimpleName(), accessToken);
                                    spreadsheetService.setAuthSubToken(accessToken);
                                    // Make a request to the API and get all spreadsheets.
                                    SpreadsheetFeed spreadsheetFeed = spreadsheetService.getFeed(spreadSheetUrl, SpreadsheetFeed.class);
                                    if (spreadsheetFeed != null){
                                        spreadsheetEntryList = spreadsheetFeed.getEntries();
                                        if (spreadsheetEntryList != null && !spreadsheetEntryList.isEmpty()){

                                            for (SpreadsheetEntry spreadsheetEntry: spreadsheetEntryList){
                                                String title = spreadsheetEntry.getTitle().getPlainText();
                                                //  Util.showToast(activity, title);
                                                Log.i(GoogleSheetUtility.class.getSimpleName(), title);
                                                if (title != null && title.equals(Constants.REQUIRED_SPREADSHEET_TITLE)){
                                                    //  Choose a spreadsheet more intelligently based on your app's needs                                              // app's needs.
                                                    requiredSpreadsheetEntry = spreadsheetEntry;
                                                    break;
                                                }
                                            }
                                            if (requiredSpreadsheetEntry == null) {
                                                message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                            }else{
                                                if (requiredSpreadsheetEntry.getCanEdit()){
                                                    worksheetFeed = spreadsheetService.getFeed(requiredSpreadsheetEntry.getWorksheetFeedUrl(), WorksheetFeed.class);
                                                    if (worksheetFeed != null){
                                                        worksheetEntryList = worksheetFeed.getEntries();
                                                        if (worksheetEntryList != null && !worksheetEntryList.isEmpty()){
                                                            for (WorksheetEntry worksheetEntry: worksheetEntryList){
                                                                String title = worksheetEntry.getTitle().getPlainText();
                                                                // Util.showToast(activity, title);
                                                                Log.i(GoogleSheetUtility.class.getSimpleName(), title);
                                                                if (title != null && title.equals(Constants.REQUIRED_ITEM_WORKSHEET_TITLE)) {
                                                                    //  Choose a worksheet more intelligently based on your app's needs.
                                                                    requiredWorksheetEntry = worksheetEntry;
                                                                    break;
                                                                }
                                                            }
                                                            if (requiredWorksheetEntry == null){
                                                                message.what = Constants.NO_SPREAD_TAB_AVAILABLE;
                                                                // Util.showToast(activity, Constants.WORK_SHEET_ENTRY_NULL);
                                                            }else {
                                                                if (requiredWorksheetEntry.getCanEdit()){
                                                                    URL listFeedUrl = requiredWorksheetEntry.getListFeedUrl();
                                                                    // Fetch the list feed of the worksheet.
                                                                    Set<String> columnsSet = null;
                                                                    if(listFeedUrl != null){
                                                                        ListFeed listFeed = spreadsheetService.getFeed(listFeedUrl, ListFeed.class);
                                                                        if(listFeed != null && listFeed.getEntries().size() > 0){
                                                                            ListEntry entry = listFeed.getEntries().get(0);
                                                                            if(entry != null){
                                                                                columnsSet = entry.getCustomElements().getTags();
                                                                            }
                                                                        }
                                                                    }
                                                                    if(columnsSet != null && columnsSet.size() > 0 && !columnsSet.contains(Constants.CHECK_COLOUMN_USER_EMAIL)){
                                                                        message.what = Constants.NO_USER_EMAIL;
                                                                    }else if(columnsSet != null && columnsSet.size() > 0 && !columnsSet.contains(Constants.CHECK_COLOUMN_DATE_TIME)){
                                                                        message.what = Constants.NO_DATE_TIME;
                                                                    }else if(columnsSet != null && columnsSet.size() > 0 && !columnsSet.contains(Constants.CHECK_COLOUMN_CATEGORY)){
                                                                        message.what = Constants.NO_CATEGORY;
                                                                    }else if(columnsSet != null && columnsSet.size() > 0 && !columnsSet.contains(Constants.CHECK_COLOUMN_ITEM_NAME)){
                                                                        message.what = Constants.NO_ITEM_NAME;
                                                                    }else if(columnsSet != null && columnsSet.size() > 0 && !columnsSet.contains(Constants.CHECK_COLOUMN_TARGET_EMAIL)){
                                                                        message.what = Constants.NO_TARGET_EMAIL;
                                                                    }else{
                                                                        // Create a local representation of the new row.
                                                                        ListEntry listEntry = new ListEntry();
                                                                        listEntry.getCustomElements().setValueLocal(Constants.COLUMN_USER_EMAIL,accountName);
                                                                        listEntry.getCustomElements().setValueLocal(Constants.COLUMN_DATE_TIME,Util.getCurrentDateTime());
                                                                        listEntry.getCustomElements().setValueLocal(Constants.COLUMN_TARGET_EMAIL,targetEmail);
                                                                        Log.i("Map Size ::;", ":::" + categoryItemMap.size());
                                                                        for (String key : categoryItemMap.keySet() ) {
                                                                            Log.i("Category Name ::;",":::"+ key);
                                                                            listEntry.getCustomElements().setValueLocal(Constants.COLUMN_CATEGORY,key);
                                                                            iteams = new ArrayList<>(0);
                                                                            if(categoryItemMap != null && categoryItemMap.size() > 0){
                                                                                iteams = categoryItemMap.get(key);
                                                                                Log.i("Item List Size ::;", ":::" + iteams.size());
                                                                                if(iteams != null && iteams.size() > 0){
                                                                                    for(String item: iteams){
                                                                                        Log.i("Item Details::::::",item);
                                                                                        listEntry.getCustomElements().setValueLocal(Constants.COLUMN_ITEM_NAME,item);
                                                                                        ListEntry resultListEntry = spreadsheetService.insert(listFeedUrl, listEntry);
                                                                                        if (resultListEntry != null){

                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                                        // Send the new row to the API for insertion.
                                                                        message.what = Constants.INSERT_ITEM_DETAILS;
                                                                   }

                                                                }else{
                                                                    message.what = Constants.NO_EDIT_ACCESS;
                                                                }
                                                            }
                                                        }else{
                                                            message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                                        }
                                                    }
                                                }else{
                                                    message.what = Constants.NO_EDIT_ACCESS;
                                                }
                                            }
                                        }else {
                                            message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                        }
                                    }else{
                                        message.what = Constants.NO_SPREAD_SHEET_AVAILABLE;
                                    }
                                }else {
                                    //calling this method in a condition when accesstoken is non null and non empty but has expired
                                    insertItemDetailSpreadSheetEntries(accountName,targetEmail,categoryItemMap,handler);
                                }
                            }else {
//                                Util.showToast(activity, Constants.ACCESS_TOKEN_NULL);
                            }
                        }else{
//                            Util.showToast(activity, Constants.TYPE_NOT_CONNECTED_STRING);
                        }
                    }

                }catch (MalformedURLException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }catch (ServiceException e){
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{
                if(progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                spreadsheetService = null;

                Bundle bundle = new Bundle();
                handler.sendMessage(message);
            }catch (Exception e){
                e.printStackTrace();
            }


        }
    }



    /*@method validateAuthTokenUtil
    * @param String
    * @return boolean
    * This method is used to check the validity of the access token that we got in the @method getAuthenticationToken. If token is
    * found to be invalid then it clears the token from local cache.
    * Returns true if token is valid and false if token is invalid.*/
    private boolean validateAuthTokenUtil(String accessToken){
        if (accessToken != null && !accessToken.equals("")){
            String url = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=" + accessToken;
            try {
                URL tokenURL = new URL(url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)tokenURL.openConnection();
                httpURLConnection.setReadTimeout(Constants.TIMEOUT_CONNECTION);
                httpURLConnection.setConnectTimeout(Constants.TIMEOUT_SOCKET);
                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK){
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder builder = new StringBuilder();
                    String line = "";
                    while((line = bufferedReader.readLine()) != null){
                        builder.append(line);
                    }
                    if (bufferedReader != null){
                        bufferedReader.close();
                    }
                    String result = builder.toString();
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject != null){
                        Log.i(GoogleSheetUtility.class.getSimpleName(),"Json Object: "+jsonObject.toString());
                        if (jsonObject.get(Constants.TAG_EXPIRES_IN) != null){
                            String expiryTime = jsonObject.getString(Constants.TAG_EXPIRES_IN);
                            if (expiryTime != null && !expiryTime.equals("")){
                                int time = Integer.valueOf(expiryTime);
                                if (time != 0){
                                    return true;
                                }
                            }
                        }else if (jsonObject.get(Constants.TAG_ERROR) != null){
                            String error = jsonObject.getString(Constants.TAG_ERROR);
                            if (error != null && !error.equals("") && error.equals(Constants.INVALID_TOKEN)){
                                try {
                                    GoogleAuthUtil.clearToken(activity, accessToken);
                                } catch (GoogleAuthException e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }
                        }
                    }
                }
            }catch (MalformedURLException e){
                if(progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                e.printStackTrace();
            }catch (IOException e){
                if(progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                e.printStackTrace();
            }catch (JSONException e){
                if(progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                e.printStackTrace();
            }
        }else {
           // Util.showToast(activity, Constants.ACCESS_TOKEN_NULL);
        }
        return false;

    }

 /*   private void sendMessage(String msg){
        Bundle bundle = new Bundle();
        Message message = new Message();
        bundle.putString("message",msg);
        message.setData(bundle);
        authHandler.sendMessage(message);

    }*/
}
